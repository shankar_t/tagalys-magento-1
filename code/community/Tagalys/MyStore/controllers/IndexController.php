<?php
class Tagalys_Mystore_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        if (Mage::helper('tagalys_core')->isTagalysModuleEnabled('mystore')) {
            $this->loadLayout();

            $head = $this->getLayout()->getBlock('head');

            $head->setTitle('My Store');
            $head->setRobots('NOINDEX,NOFOLLOW');

            $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'search',
                array('template' => 'tagalys/my_store.phtml')
            );
            $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
            $this->getLayout()->getBlock('content')->append($block);
            $this->_initLayoutMessages('core/session');
            $this->renderLayout();
        } else {
            $this->norouteAction();
            return;
        }
    }

}