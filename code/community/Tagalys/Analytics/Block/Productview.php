<?php 
    class Tagalys_Analytics_Block_Productview extends Mage_Catalog_Block_Product_View {
        public function getMainProduct() {
          $mainProduct = Mage::registry('current_product');
          if (is_object($mainProduct)) {
            return $mainProduct;
          }
          return false;
        }
    }