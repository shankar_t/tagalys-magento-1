<?php
class Tagalys_Analytics_Model_Observer  extends Varien_Object {
    public function addToCart(Varien_Event_Observer $observer) {
        try {
            $trackedProduct = $observer->getProduct();
            $productType = $trackedProduct->getTypeId();
            $analyticsCookieData = array(1 /* cookie format version */, 'product_action', 'add_to_cart', array(array(Mage::app()->getRequest()->getParam('qty', 1))));
            if ($productType == 'configurable') {
                $simpleProduct = $observer->getEvent()->getQuoteItem()->getProduct();
                $analyticsCookieData[3][0][] = $trackedProduct->getId();
                $analyticsCookieData[3][0][] = $simpleProduct->getId();
            } else if ($productType == 'simple') {
                $parentId = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($trackedProduct->getId());
                if(isset($parentId[0])) {
                    // simple product of configurable product
                    $configurableProduct = Mage::getModel('catalog/product')->load($parentId[0]);
                    $analyticsCookieData[3][0][] = $configurableProduct->getId();
                    $analyticsCookieData[3][0][] = $trackedProduct->getId();
                } else {
                    // plain simple product
                    $analyticsCookieData[3][0][] = $trackedProduct->getId();
                }
            } else {
                // other product type
                $analyticsCookieData[3][0][] = $trackedProduct->getId();
            }
            if ($productType == 'simple') {
                
            }
            Mage::getModel('core/cookie')->set('__ta_event', json_encode($analyticsCookieData));
        } catch (Exception $e) {
            
        }
    }

    public function orderSuccess(Varien_Event_Observer $observer) {
        try {
            $orderIds = $observer->getEvent()->getOrderIds();
            if (empty($orderIds) || !is_array($orderIds)) {
                return;
            }
            $orderId = $orderIds[0];
            $order = Mage::getModel('sales/order')->load($orderId);
            $analyticsCookieData = array(1 /* cookie format version */, 'product_action', 'buy', array(), $orderId);

            foreach($order->getAllVisibleItems() as $item){
                $product = Mage::helper('tagalys_core')->getProductFromItem($item);
                if ($product->isConfigurable()) {
                    $children = $item->getChildrenItems();
                    if(count($children) > 0) {
                        foreach ($children as $childItem) {
                            $analyticsCookieData[3][] = array((int) $item->getQtyOrdered(), $product->getId(), $childItem->getProduct()->getId());
                        }
                    } else {
                        $analyticsCookieData[3][] = array((int) $item->getQtyOrdered(), $product->getId(), $item->getProduct()->getId());
                    }
                } else {
                    $analyticsCookieData[3][] = [(int) $item->getQtyOrdered(), $product->getId()];
                }
            }
            Mage::register('tagalys_analytics_event', json_encode($analyticsCookieData));
        } catch (Exception $e) {
            
        }
    }

    public function customerLogin(Varien_Event_Observer $observer) {
        try {
            $customerId = $observer->getCustomer()->getId();
            // don't use magento's cookie helper as it sets httponly header to true
            setcookie('__ta_logged_in', $customerId, time()+60*60*24*3, '/', $_SERVER['HTTP_HOST']);
        } catch (Exception $e) {
            
        }
    }

    public function beforeDispatch(Varien_Event_Observer $observer) {
        try {
            $customerSession = Mage::getSingleton('customer/session');
            if($customerSession->isLoggedIn()) {
                $customerId = $customerSession->getCustomer()->getId();
                // don't use magento's cookie helper as it sets httponly header to true
                setcookie('__ta_user_id', $customerId, time()+60*60*24*3, '/', $_SERVER['HTTP_HOST']);
            } else {
                setcookie("__ta_user_id", "", time()-3600, '/', $_SERVER['HTTP_HOST']);
            }
        } catch (Exception $e) {
            
        }
    }
}