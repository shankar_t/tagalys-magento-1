<?php
$installer = $this;

$installer->startSetup();

if (!$installer->getConnection()->isTableExists($installer->getTable('tagalys_mpages_cache'))) {
    $tagalys_mpages_cache_table = $installer->getConnection()->newTable($installer->getTable('tagalys_mpages_cache'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
            ), 'ID')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'nullable' => false,
            ), 'Store ID')
        ->addColumn('url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => '-',
            ), 'Mpage URL component')
        ->addColumn('data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            'default' => '',
            ), 'Cache data in JSON');
    $tagalys_mpages_cache_table->addIndex(
      $installer->getIdxName('tagalys_mpages/cache', array('store_id', 'url'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
      array('store_id', 'url'),
      array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
    $installer->getConnection()->createTable($tagalys_mpages_cache_table);
}

$installer->endSetup();
