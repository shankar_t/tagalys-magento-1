<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropIndex(
    $installer->getTable('tagalys_mpages_cache'),
    $installer->getIdxName('tagalys_mpages/cache', array('url'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
);
$installer->getConnection()->addIndex(
    $installer->getTable('tagalys_mpages_cache'),
    $installer->getIdxName('tagalys_mpages/cache', array('store_id', 'url'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    array('store_id', 'url')
);

$installer->endSetup();