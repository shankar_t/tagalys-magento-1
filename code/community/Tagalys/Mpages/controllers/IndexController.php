<?php
class Tagalys_Mpages_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();

        if (Mage::helper('tagalys_core')->isTagalysModuleEnabled('mpages')) {
            try {
                $params = Mage::app()->getRequest()->getParams();

                if (array_key_exists('f', $params) || array_key_exists('sort', $params) || array_key_exists('page', $params)) {
                    $this->getLayout()->getBlock('head')->setRobots('NOINDEX,NOFOLLOW');
                }
                
                $response = Mage::helper('tagalys_mpages')->getMpageData(Mage::app()->getStore()->getId(), $params['mpage']);

                if ($response !== false) {
                    $head = $this->getLayout()->getBlock('head');
                    if ($head) {
                        if (isset($response['variables'])) {
                            if (isset($response['variables']['page_title']) && $response['variables']['page_title'] != '' ) {
                                $head->setTitle($response['variables']['page_title']);
                            } else {
                                $head->setTitle($response['name']);
                            }
                            if (isset($response['variables']['meta_keywords'])) {
                                $head->setKeywords($response['variables']['meta_keywords']);
                            }
                            if (isset($response['variables']['meta_description'])) {
                                $head->setDescription($response['variables']['meta_description']);
                            }
                            if (isset($response['variables']['meta_robots'])) {
                                $head->setRobots($response['variables']['meta_robots']);
                                if (array_key_exists('f', $params) || array_key_exists('sort', $params) || array_key_exists('page', $params)) {
                                    $this->getLayout()->getBlock('head')->setRobots('NOINDEX,NOFOLLOW');
                                }
                            }
                        }
                        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
                        $exploded = explode('?', $currentUrl);
                        $head->addLinkRel('canonical', $exploded[0]);
                    }
                } else {
                    $this->norouteAction();
                    return;
                }
            } catch (Exception $e) {
                
            }

            $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'search',
                array('template' => 'tagalys/mpage.phtml')
            );
            $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
            $this->getLayout()->getBlock('content')->append($block);
            $this->_initLayoutMessages('core/session');
            $this->renderLayout();
        } else {
            $this->norouteAction();
            return;
        }
    }
}