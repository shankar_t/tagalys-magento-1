<?php
class Tagalys_Mpages_Helper_Data extends Mage_Core_Helper_Abstract {
    public function updateMpagesCache() {
        try {
            $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
            $setup_complete = ($setup_status == 'completed');
            if ($setup_complete) {
                $stores_for_tagalys = Mage::helper('tagalys_core')->getStoresForTagalys();
                if ($stores_for_tagalys != null) {
                    foreach ($stores_for_tagalys as $store_id) {
                        $processed = array();
                        $page = 1;
                        $per_page = 50;
                        $response = Mage::getSingleton('tagalys_core/client')->storeApiCall($store_id, '/v1/mpages', array("page" => $page, "per_page" => $per_page, "request" => ["url_component", "variables", "total"]));
                        $processed_now = $this->processResponse($store_id, $response);
                        $processed = array_merge($processed, $processed_now);
                        $finished = (($page - 1) * $per_page) + count($response['results']);
                        $loop_number = 1;
                        while ($finished < $response['total'] || $loop_number >= 50) {
                            $page += 1;
                            $response = Mage::getSingleton('tagalys_core/client')->storeApiCall($store_id, '/v1/mpages', array("page" => $page, "per_page" => $per_page, "request" => ["url_component", "variables", "total"]));
                            $processed_now = $this->processResponse($store_id, $response);
                            $processed = array_merge($processed, $processed_now);
                            $finished = (($page - 1) * $per_page) + count($response['results']);
                            $loop_number += 1;
                        }
                        Mage::getModel('tagalys_mpages/cache')->deleteStoreUrlsExcept($store_id, $processed);
                    }
                }
            }
        } catch (Exception $e) {
            Mage::log("Error in updateMpagesCache: ". $e->getMessage(), null, "tagalys_core.log");
        }
    }

    protected function processResponse($store_id, $response) {
        $ids = array();
        foreach($response['results'] as $result) {
            $saveResponse = Mage::getModel('tagalys_mpages/cache')->saveMpageCache($store_id, $result);
            if ($saveResponse != false) {
                array_push($ids, $saveResponse);
            }
        }
        return $ids;
    }

    public function getMpageData($store_id, $mpage) {
        $getMpage = Mage::getModel('tagalys_mpages/cache')->getCollection()->addFieldToFilter('store_id', Mage::app()->getStore()->getId())->addFieldToFilter('url', $mpage);
        $where = $getMpage->getSelect()->getPart('where');
        $newWhere = str_replace('\''.$mpage.'\'','BINARY \''.$mpage.'\'', $where);
        $getMpage->getSelect()->setPart('where', $newWhere);
        $mpage_collection = $getMpage->getFirstItem();
        if ($id = $mpage_collection->getId()) {
            $mpage_data = $mpage_collection->getData('data');
            $response = json_decode($mpage_data, true);
        } else {
            $response = false;
            // page doesn't exist in cache. do not query live. if the page exists, Tagalys should send a request to create the cache entry.
        }
        return $response;
    }
    public function updateSpecificMpageCache($store_id, $mpage) {
        $response = Mage::getSingleton('tagalys_core/client')->storeApiCall($store_id.'', '/v1/mpages/'.$mpage, array('request' => array('variables','total')));
        if ($response != false) {
            $response['url_component'] = $mpage;
            Mage::getModel('tagalys_mpages/cache')->saveMpageCache($store_id, $response);
        }
        return $response;
    }
    public function saveCacheData($params) {
        $params_for_model = array();
        // try finding the record with url_component_was, if specified
        if (array_key_exists('url_component_was', $params)) {
            $mpage_cache_to_update = Mage::getModel('tagalys_mpages/cache')->getCollection()->addFieldToFilter('store_id', $params['store_id'])->addFieldToFilter('url', $params['url_component_was'])->getFirstItem();
            if ($mpage_cache_to_update->getId()) {
                $params_for_model['url_component_was'] = $params['url_component_was'];
            }
        }
        $params_for_model['url_component'] = $params['url_component'];
        $params_for_model['platform'] = $params['platform'];
        $params_for_model['name'] = $params['cache_data']['name'];
        $params_for_model['variables'] = $params['cache_data']['variables'];
        Mage::getModel('tagalys_mpages/cache')->saveMpageCache($params['store_id'], $params_for_model);
        return $params_for_model;
    }
    public function deleteCacheData($params) {
        $response = Mage::getModel('tagalys_mpages/cache')->deleteMpageCache($params['store_id'], $params['url_component']);
        return $response;
    }
}
