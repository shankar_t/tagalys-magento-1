<?php
class Tagalys_Mpages_Model_Observer {
    
    public function updateMpagesCacheCron() { 
        try {
            $utc_now = new DateTime("now", new DateTimeZone('UTC'));
            $time_now = $utc_now->format(DateTime::ATOM);
            Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:updateMpagesCacheCron", $time_now);
            Mage::helper("tagalys_mpages")->updateMpagesCache();
        } catch (Exception $e) {
            Mage::log("Error in updateMpagesCacheCron: ". $e->getMessage(), null, "tagalys_core.log");
        }
        return $this;
    }
}