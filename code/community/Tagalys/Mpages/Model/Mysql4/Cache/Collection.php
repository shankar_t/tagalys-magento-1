<?php
class Tagalys_Mpages_Model_Mysql4_Cache_collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
    protected function _construct() {
        $this->_init("tagalys_mpages/cache");
    }
}