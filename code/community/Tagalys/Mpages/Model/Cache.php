<?php
class Tagalys_Mpages_Model_Cache extends Mage_Core_Model_Abstract {
    public function _construct() {
        parent::_construct();
        $this->_init("tagalys_mpages/cache");
    }
    public function saveMpageCache($store_id, $response) {
        $data = array('store_id' => $store_id, 'url' => $response['url_component'], 'data' => json_encode($response));

        $url_component_to_search = $response['url_component'];
        if (array_key_exists('url_component_was', $response)) {
            $url_component_to_search = $response['url_component_was'];
        }

        $collection = $this->getCollection()->addFieldToFilter('store_id', $store_id)->addFieldToFilter('url', $url_component_to_search)->getFirstItem();

        try {
            if ($id = $collection->getId()){
                $model = $this->load($id)->addData($data);
                $model->setId($id)->save();
                return $id;
            } else {
                $model = Mage::getModel("tagalys_mpages/cache")->setData($data);
                $insertId = $model->save()->getId();
                return $insertId;
            }
        } catch (Exception $e){
            Mage::getSingleton('tagalys_core/client')->log('error', 'Exception in saveMpageCache', array('exception_message' => $e->getMessage()));
            return false;
        }
    }
    public function deleteMpageCache($store_id, $url_component) {
        $mpage_cache_to_delete = $this->getCollection()->addFieldToFilter('store_id', $store_id)->addFieldToFilter('url', $url_component)->getFirstItem();
        if ($id = $mpage_cache_to_delete->getId()) {
            $mpage_cache_to_delete->delete();
            return true;
        } else {
            return false;
        }
    }
    public function deleteStoreUrlsExcept($store_id, $ids) {
      if (count($ids) == 0) {
        $itemsToDelete = $this->getCollection()->addFieldToFilter('store_id', $store_id)->addFieldToFilter('id', array('nin' => array(0)));
      } else {
        $itemsToDelete = $this->getCollection()->addFieldToFilter('store_id', $store_id)->addFieldToFilter('id', array('nin' => $ids));
      }
      foreach ($itemsToDelete as $itemToDelete) {
          $itemToDelete->delete();
      }
    }
}