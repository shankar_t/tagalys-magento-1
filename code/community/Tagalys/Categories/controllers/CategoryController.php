<?php
require_once(Mage::getModuleDir('controllers','Mage_Catalog').DS.'CategoryController.php');
class Tagalys_Categories_CategoryController extends Mage_Catalog_CategoryController
{
    /**
     * Category view action
     */
    public function viewAction()
    {
        try {
            $storeId = Mage::app()->getStore()->getStoreId();
            if ($this->jsRenderingEnabled($storeId)) {
                $categoryId = (int) $this->getRequest()->getParam('id', false);
                if (!Mage::helper("tagalys_core/categories")->uiPoweredByTagalys($storeId, $categoryId)) {
                    return $this->fallbackToParent();
                }
            } else {
                return $this->fallbackToParent();
            }
        } catch (Exception $e) {
            return $this->fallbackToParent();
        }
        if ($category = $this->_initCatagory()) {
            $design = Mage::getSingleton('catalog/design');
            $settings = $design->getDesignSettings($category);

            // apply custom design
            if ($settings->getCustomDesign()) {
                $design->applyCustomDesign($settings->getCustomDesign());
            }

            Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

            $update = $this->getLayout()->getUpdate();
            $update->addHandle('default');

            if (!$category->hasChildren()) {
                $update->addHandle('catalog_category_layered_nochildren');
            }

            $this->addActionLayoutHandles();
            $update->addHandle($category->getLayoutUpdateHandle());
            $update->addHandle('CATEGORY_' . $category->getId());
            $this->loadLayoutUpdates();

            // apply custom layout update once layout is loaded
            if ($layoutUpdates = $settings->getLayoutUpdates()) {
                if (is_array($layoutUpdates)) {
                    foreach($layoutUpdates as $layoutUpdate) {
                        $update->addUpdate($layoutUpdate);
                    }
                }
            }

            $this->generateLayoutXml()->generateLayoutBlocks();
            // apply custom layout (page) template once the blocks are generated
            if ($settings->getPageLayout()) {
                $this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
            }

            if (Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:override_layout') == '1') {
                $this->getLayout()->helper('page/layout')->applyTemplate(Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:override_layout_name'));
            }

            if ($root = $this->getLayout()->getBlock('root')) {
                $root->addBodyClass('categorypath-' . $category->getUrlPath())
                    ->addBodyClass('category-' . $category->getUrlKey());
            }

            $this->getLayout()->getBlock('category.products')->setTemplate('tagalys/category.phtml');

            try {
                $params = Mage::app()->getRequest()->getParams();
                if (array_key_exists('f', $params) || array_key_exists('sort', $params) || array_key_exists('page', $params)) {
                    $this->getLayout()->getBlock('head')->setRobots('NOINDEX,FOLLOW');
                }
            } catch (Exception $e) {

            }

            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('checkout/session');
            $this->renderLayout();
        }
        elseif (!$this->getResponse()->isRedirect()) {
            $this->_forward('noRoute');
        }
    }

    public function fallbackToParent() {
        return parent::viewAction();
    }

    public function jsRenderingEnabled($storeId){
        if (Mage::getModel('tagalys_core/config')->getTagalysConfig('tagalys:health') != '1') {
            return false;
        }
        $jsRenderedStores = Mage::helper('tagalys_core')->getJsRenderedStores();
        return in_array($storeId, $jsRenderedStores);
    }
}