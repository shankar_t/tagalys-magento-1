<?php
class Tagalys_Core_Model_ProductDetails extends Mage_Core_Model_Abstract {
    
    protected $syncfield;
    protected $inventorySyncField;

    public function getProductFields($productId, $storeId, $forceRegenerateThumbnail) {
        $this->_storeId = $storeId;
        Mage::app()->setCurrentStore($this->_storeId);
        $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
        $productFields = new stdClass();
        $productFields->__id = $product->getId();
        $productFields->__magento_type = $product->getTypeId();
        $productFields->name = $product->getName();
        $productFields->link = $product->getProductUrl();
        $productFields->sku = $product->getData('sku');
        $attributes = $product->getTypeInstance(false)->getEditableAttributes($product);
        $productFields = $this->addProductRatingFields($productFields);
        foreach ($attributes as $attribute) {
            $is_for_display = ((bool)$attribute->getUsedInProductListing() && (bool)$attribute->getIsUserDefined());
            if ($attribute->getIsFilterable() || $attribute->getIsSearchable() || $is_for_display) {
                $attr = $product->getResource()->getAttribute($attribute->getAttributeCode());
                if ($attr->getFrontendInput() != "select" && $attr->getFrontendInput() != "multiselect") {
                    if($attr->getFrontendInput() == 'date') {
                        $fieldVal = $product->getData($attribute->getAttributeCode());
                    } else {
                        $fieldVal = $attribute->getFrontend()->getValue($product);
                    }
                    if (!is_null($fieldVal)) {
                        $productFields->{$attribute->getAttributeCode()} = $fieldVal;
                    }
                }
            }
        }
        $productType = $product->getTypeId();
        switch($productType) {
            case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
                $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
                foreach($associatedProducts as $associatedProduct) {
                    $price[] = $associatedProduct->getFinalPrice();
                    $price[] = $this->getFinalPriceForProduct($this->_storeId, $associatedProduct);
                    $mrp[] = $associatedProduct->getPrice();
                }
                $productFields->sale_price = min($price);
                $productFields->price = min($mrp);
                break;
            case Mage_Catalog_Model_Product_Type::TYPE_BUNDLE:
                // check the price for bundle products in multi store (this is using catalog_flat collection internally)
                $prices = Mage::getModel('bundle/product_price')->getTotalPrices($product,null,1);
                $productFields->sale_price = $prices[0]; // from price
                $productFields->price = $prices[0]; // from price
                $productFields->sale_price_max = $prices[1]; // to price
                break;
            default:
                $prices = $this->getProductPrices($this->_storeId, $product);
                $productFields->sale_price = $prices['sale_price'];
                $productFields->price = $prices['price'];
                if(array_key_exists('sale_price_max', $prices)) {
                    $productFields->sale_price_min = $prices['sale_price_min'];
                    $productFields->price_min = $prices['price_min'];
                    $productFields->sale_price_max = $prices['sale_price_max'];
                    $productFields->price_max = $prices['price_max'];
                }
                break;
        }
        if ($productFields->price === false) {
            $productFields->price = $productFields->sale_price;
        }

        // if special price from date exists
            // if current date >= special price from date
                // if special price to date exists
                    // if current date <= special price to date
                        // save special to field
                    // else
                        // ignore from/to fields
                    // end
                // else
                    // keep sale_price as is
                // end
            // else
                // set other_sale_price = sale_price
                // save from/to fields
            // end
        // end
        $productFields->scheduled_updates = array();
        if ($product->getSpecialFromDate() != null) {
            
            $special_price_from_datetime = new DateTime($product->getSpecialFromDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
            $current_datetime = new DateTime("now", new DateTimeZone('UTC'));
            if ($current_datetime->getTimestamp() >= $special_price_from_datetime->getTimestamp()) {
                if ($product->getSpecialToDate() != null) {
                    $special_price_to_datetime = new DateTime($product->getSpecialToDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                    if ($current_datetime->getTimestamp() <= ($special_price_to_datetime->getTimestamp() + 24*60*60 - 1)) {
                        // sale price is currently valid. record to date
                        array_push($productFields->scheduled_updates, array('at' => str_replace('00:00:00', '23:59:59', $special_price_to_datetime->format('Y-m-d H:i:sP')), 'updates' => array('sale_price' => $productFields->price)));
                    } else {
                        // sale is past expiry; don't record from/to datetimes
                    }
                } else {
                    // sale price is valid indefinitely; make no changes;
                }
            } else {
                // future sale - record other sale price and from/to datetimes
                $specialPrice = $product->getSpecialPrice();
                if ($specialPrice != null && $specialPrice > 0) {
                    $special_price_from_datetime = new DateTime($product->getSpecialFromDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                    array_push($productFields->scheduled_updates, array('at' => $special_price_from_datetime->format('Y-m-d H:i:sP'), 'updates' => array('sale_price' => $product->getSpecialPrice())));
                    if ($product->getSpecialToDate() != null) {
                        $special_price_to_datetime = new DateTime($product->getSpecialToDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                        array_push($productFields->scheduled_updates, array('at' => str_replace('00:00:00', '23:59:59', $special_price_to_datetime->format('Y-m-d H:i:sP')), 'updates' => array('sale_price' => $productFields->price)));
                    }
                }
            }
        }

        // New
        $current_datetime = new DateTime("now", new DateTimeZone('UTC'));
        $productFields->__new = false;
        if ($product->getNewsFromDate() != null) {
            $new_from_datetime = new DateTime($product->getNewsFromDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
            if ($current_datetime->getTimestamp() >= $new_from_datetime->getTimestamp()) {
                if ($product->getNewsToDate() != null) {
                    $new_to_datetime = new DateTime($product->getNewsToDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                    if ($current_datetime->getTimestamp() <= ($new_to_datetime->getTimestamp() + 24*60*60 - 1)) {
                        // currently new. record to date
                        $productFields->__new = true;
                        array_push($productFields->scheduled_updates, array('at' => str_replace('00:00:00', '23:59:59', $new_to_datetime->format('Y-m-d H:i:sP')), 'updates' => array('__new' => false)));
                    } else {
                        // new is past expiry; don't record from/to datetimes
                    }
                } else {
                    // new is valid indefinitely
                    $productFields->__new = true;
                }
            } else {
                // new in the future - record from/to datetimes
                array_push($productFields->scheduled_updates, array('at' => $new_from_datetime->format('Y-m-d H:i:sP'), 'updates' => array('__new' => true)));
                if ($product->getNewsToDate() != null) {
                    $new_to_datetime = new DateTime($product->getNewsToDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                    array_push($productFields->scheduled_updates, array('at' => str_replace('00:00:00', '23:59:59', $new_to_datetime->format('Y-m-d H:i:sP')), 'updates' => array('__new' => false)));
                }
            }
        } else {
            // no from date. assume applicable from now
            if ($product->getNewsToDate() != null) {
                $new_to_datetime = new DateTime($product->getNewsToDate(), new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                if ($current_datetime->getTimestamp() <= ($new_to_datetime->getTimestamp() + 24*60*60 - 1)) {
                    // currently new. record to date
                    $productFields->__new = true;
                    array_push($productFields->scheduled_updates, array('at' => str_replace('00:00:00', '23:59:59', $new_to_datetime->format('Y-m-d H:i:sP')), 'updates' => array('__new' => false)));
                } else {
                    // new is past expiry; don't record from/to datetimes
                }
            }
        }

        $utc_now = new DateTime("now", new DateTimeZone('UTC'));
        $time_now =  $utc_now->format(DateTime::ATOM);
        $productFields->synced_at = $time_now;
        $productFields->image_url = $this->getProductImageUrl(Mage::getModel('tagalys_core/config')->getTagalysConfig("product_image_attribute"), $product, $storeId, $forceRegenerateThumbnail);
        if (Mage::getModel('tagalys_core/config')->getTagalysConfig("product_image_hover_attribute") != '') {
            $productFields->image_hover_url = $this->getProductImageUrl(Mage::getModel('tagalys_core/config')->getTagalysConfig("product_image_hover_attribute"), $product, $storeId, $forceRegenerateThumbnail);
        }
        $fields = array('created_at');
        foreach ($fields as $key => $name) {
            $fieldValue = $product->getResource()->getAttribute($name)->getFrontend()->getValue($product);
            $introduced_at = new DateTime((string)$fieldValue);
            $introduced_at->setTimeZone(new DateTimeZone('UTC'));
            $productFields->introduced_at = $introduced_at->format(DateTime::ATOM);
        }
        $productFields->in_stock = Mage::getModel('catalog/product')->load($product->getId())->isSaleable();

        return $productFields;
    }

    public function getProductPrices($storeId, $product) {
        $prices = [
            'price' => $product->getPrice(),
            'sale_price' => $this->getFinalPriceForProduct($storeId, $product),
        ];
        if(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE == $product->getTypeId()) {
            $tagalysConfiguration = Mage::getModel('tagalys_core/config');
            $syncMinMaxPrices = $tagalysConfiguration->getTagalysConfig('sync:get_min_max_prices_for_configurable_products', true);
            if($syncMinMaxPrices) {
                $minMaxPrices = $this->getMinMaxPricesForConfigurableProduct($storeId, $product);
                if($minMaxPrices) {
                    $prices['price_min'] = $minMaxPrices['price_min'];
                    $prices['sale_price_min'] = $minMaxPrices['sale_price_min'];
                    $prices['price_max'] = $minMaxPrices['price_max'];
                    $prices['sale_price_max'] = $minMaxPrices['sale_price_max'];
                }
            }
        }
        return $prices;
    }

    public function getMinMaxPricesForConfigurableProduct($storeId, $product) {
        $priceMax = 0;
        $salePriceMax = 0;
        $priceMin = PHP_INT_MAX;
        $salePriceMin = PHP_INT_MAX;
        $result = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
        if(!empty($result)) {
            $childrenIds = $result[0];
            foreach($childrenIds as $childId) {
                $childProduct = Mage::getModel('catalog/product')->load($childId);
                $salePrice = (float)$this->getFinalPriceForProduct($storeId, $childProduct);
                if($salePrice < $salePriceMin) {
                    $salePriceMin = $salePrice;
                    $priceMin = (float)$childProduct->getPrice();
                }
                if($salePrice > $salePriceMax) {
                    $salePriceMax = $salePrice;
                    $priceMax = (float)$childProduct->getPrice();
                }
            }
            return [
                'price_min' => $priceMin,
                'sale_price_min' => $salePriceMin,
                'price_max' => $priceMax,
                'sale_price_max' => $salePriceMax
            ];
        }
        return false;
    }

    public function getFinalPriceForProduct($storeId, $product) {
        $finalPrice = $product->getFinalPrice();
        $rulePrice = Mage::getResourceModel('catalogrule/rule')->getRulePrice(
            Mage::app()->getLocale()->storeTimeStamp($storeId),
            Mage::app()->getStore($storeId)->getWebsiteId(),
            Mage_Customer_Model_Group::NOT_LOGGED_IN_ID,
            $product->getId()
        );
        if ($rulePrice !== false) {
            $finalPrice = min($product->getFinalPrice(), $rulePrice);
        }
        return $finalPrice;
    }
    public function addProductRatingFields($productFields) {
        $ratings = Mage::getModel('rating/rating')->getResourceCollection();

        $reviews = Mage::getModel('review/review')->getResourceCollection()->addStoreFilter( Mage::app()->getStore()->getId() )
          ->addEntityFilter('product', $productFields->__id)
          ->addStatusFilter( Mage_Review_Model_Review::STATUS_APPROVED )
          ->setDateOrder()
          ->addRateVotes();
        $avg = 0;
        $productRatings = array();
        foreach($ratings->getItems() as $rating) {
            $productRatings['id_'.$rating->getId()] = array();
        }
        $productRatingsCount = 0;
        if (count($reviews) > 0) {
          foreach($reviews->getItems() as $review) {
            foreach( $review->getRatingVotes() as $vote ) {
                $productRatings['id_'.$vote->getRatingId()][] = $vote->getPercent();
            }
          }
          $productRatingsCount = count($reviews);
        }
        $productFields->__magento_ratings_count = $productRatingsCount;
        if (count($reviews) > 0) {
            foreach($ratings->getItems() as $rating) {
                $productFields->{'__magento_avg_rating_id_'.$rating->getId()} = round((array_sum($productRatings['id_'.$rating->getId()]) / $productRatingsCount));
            }
        } else {
            foreach($ratings->getItems() as $rating) {
                $productFields->{'__magento_avg_rating_id_'.$rating->getId()} = 0;
            }
        }
        return $productFields;
    }
    public function getProductType($productId) {
        $product = Mage::getModel('catalog/product')->load($productId);
        $productType = $product->getTypeId();
        return $productType;
    }
    public function getProductParent($productId) {
        $tagalys_parent_id = array();
        $product = Mage::getModel('catalog/product')->load($productId);
        $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($productId);
        if (!$parentIds) {
            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);  
            $parentProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToFilter('entity_id', array('in' => $parentIds))
            ->load();
            $parents = array();
            foreach ($parentProducts as $key => $pro) {
                array_push($parents, $pro->getId());
            }
            if (isset($parents[0])) {
                $tagalys_parent_id = $parents;
            } 
        } 
        return $tagalys_parent_id;
    }
    public function getAttributeItemsForProduct($storeId, $product, $attribute) {
        $items = array();
        $value = (string) $product->getData($attribute->getAttributeCode());
        // For field_type == 'multiselect', $ids will have multiple values and for select, it will have a single value.
        $ids = array_filter(explode(',', $value), function($id) {
            return !empty($id) && (int) $id != 0;
        });
        $values = implode(',',$ids);
        if(!empty($values)){
            $tableName = Mage::getSingleton('core/resource')->getTableName('eav_attribute_option_value');
            $sql = "SELECT * FROM $tableName WHERE option_id IN ($values) AND store_id IN ($storeId, 0) ORDER BY store_id DESC";
            $rows = Mage::helper('tagalys_core/service')->runSqlSelect($sql);
            if (count($rows) > 0) {
                foreach ($ids as $id) {
                    // findByKey returns the first occurrence in store id desc order (data of $storeId if available or else the data of store 0 )
                    $row = $this->findByKey('option_id', $id, $rows);
                    if ($row != false) {
                        $items[] = array('id' => $id, 'label' => $row['value']);
                    }
                }
            }
        }
        return $items;
    }

    public function findByKey($key, $value, $rows){
        foreach ($rows as $row) {
            if (array_key_exists($key, $row) && $row[$key] == $value){
                return $row;
            }
        }
        return false;
    }

    public function filterDuplicateEntries($array, $key = 'id') {
        $temp_array = array();
        $i = 0;
        $key_array = array();
        
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                array_push($temp_array, $val);
            }
            $i++;
        }
        return $temp_array;
    }

    public function getDirectAttributes($storeId, $product, $rawProductAttributes, $includeVisibilityAttribute = true, $configurableAttributeCodes = null) {
        $attributes = $product->getTypeInstance(false)->getEditableAttributes($product);
        foreach ($attributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            
            // If configurableAttributeCodes is set, only include those attributes
            if ($configurableAttributeCodes && !in_array($attributeCode, $configurableAttributeCodes)) {
                continue;
            }

            if (Mage::helper('tagalys_core')->shouldSyncAttribute($attribute, $includeVisibilityAttribute)) {
                $product_attribute = $product->getResource()->getAttribute($attribute->getAttributeCode());
                $isTagSet = $product_attribute->usesSource();
                if ($isTagSet) {
                    $items = $this->getAttributeItemsForProduct($storeId, $product, $attribute);
                    if (count($items) > 0) {
                        if (is_null($rawProductAttributes[$attributeCode])) {
                            $rawProductAttributes[$attributeCode] = [
                                "label" => $this->getAttributeLabel($storeId, $attribute),
                                "items" => []
                            ];
                        }
                        $rawProductAttributes[$attributeCode]['items'] = array_merge($rawProductAttributes[$attributeCode]['items'], $items);
                    }
                }
            }
        }
        return $rawProductAttributes;
    }

    public function getAttributeLabel($storeId, $attribute) {
        $attributeId = $attribute->getId();
        if (!empty($attributeId)) {
            $tableName = Mage::getSingleton('core/resource')->getTableName('eav_attribute_label');
            $sql = "SELECT * FROM $tableName WHERE attribute_id=$attributeId AND store_id=$storeId";
            $rows = Mage::helper('tagalys_core/service')->runSqlSelect($sql);
            if (count($rows) > 0){
                // return store value
                return $rows[0]['value'];
            }
        }
        // return admin value
        return $attribute->getFrontendLabel();
    }

    public function getConfigurableAttributes($simpleProduct, $configurableAttributeCodes, $rawProductAttributes) {
        // currently not used
        foreach($configurableAttributeCodes as $attributeCode) {
            if (is_null($rawProductAttributes[$attributeCode])) {
                $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
                $rawProductAttributes[$attributeCode] = [
                    "label" => $attribute->getFrontend()->getLabel($simpleProduct),
                    "items" => []
                ];
            }
            $rawProductAttributes[$attributeCode]['items'][] = [
                'id' => $simpleProduct->getData($attributeCode),
                'label' => $simpleProduct->getAttributeText($attributeCode)
            ];
        }
        return $rawProductAttributes;
    }

    public function shouldIncludeProductTags($product, $includeOutOfStock) {
        if($includeOutOfStock) {
            return ($product->getStatus() == '1');
        } else {
            return $product->isSaleable();
        }
    }

    public function getProductAttributes($productId, $store_id, $unsyncFields) {
        Mage::app()->setCurrentStore($store_id);
        $product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($productId);
        $type = $product->getTypeId();
        $attributeObj = array();
        $categories = Mage::helper('tagalys_core')->getProductCategories($productId, $store_id);
        if (count($categories) > 0) {
            $attributeObj[] = array("tag_set" => array("id" => "__categories", "label" => "Categories" ), "items" => ($categories));
        }

        $rawProductAttributes = [];
        $rawProductAttributes = $this->getDirectAttributes($store_id, $product, $rawProductAttributes);
        if ($type === "configurable") {
            $includeOutOfStock = Mage::getModel('tagalys_core/config')->getTagalysConfig('sync:include_out_of_stock_simple_products', true);
            $configurableProduct = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
            $configurableAttributes = $configurableProduct->getConfigurableAttributes()->getItems();
            $configurableAttributeCodes = array_map(function($attribute) {
                return $attribute->getProductAttribute()->getAttributeCode();
            }, $configurableAttributes);
            $simpleProductsCollection = $configurableProduct->getUsedProductCollection()->addAttributeToFilter('status', 1)->addAttributeToSelect('*')->addFilterByRequiredOptions();
            foreach($simpleProductsCollection as $simpleProduct){
                if ($this->shouldIncludeProductTags($simpleProduct, $includeOutOfStock)) {
                    $rawProductAttributes = $this->getDirectAttributes($store_id, $simpleProduct, $rawProductAttributes, false, $configurableAttributeCodes);
                }
            }
        }

        foreach ($rawProductAttributes as $id => $details) {
            $attributeObj[] = [
                "tag_set" => [
                    "id" => $id,
                    "label" => $details['label']
                ],
                "items" => $this->filterDuplicateEntries($details['items'])
            ];
        }

        return $attributeObj;
    }

    public function getProductImageUrl($image_attribute_code, $product, $storeId, $forceRegenerateThumbnail) {
        try {
            $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $image_attribute_code);
            $productImagePath = $attribute->getFrontend()->getValue($product);
            $thisProductImagePath = Mage::getBaseDir('media') . DS . "catalog" . DS . "product" . $productImagePath;
            if(file_exists($thisProductImagePath)) {
                $imageDetails = getimagesize($thisProductImagePath);
                $width = $imageDetails[0];
                $height = $imageDetails[1];
                if ($width > 1 && $height > 1) {
                    $resizedProductImagePath = Mage::getBaseDir('media') . DS . 'tagalys' . DS . 'product_thumbnails' . $productImagePath;
                    if ($forceRegenerateThumbnail || !file_exists($resizedProductImagePath)) {
                        if (file_exists($resizedProductImagePath)) {
                            unlink($resizedProductImagePath);
                        }
                        $tagalysConfiguration = Mage::getModel('tagalys_core/config');
                        $max_product_thumbnail_width = $tagalysConfiguration->getTagalysConfig('max_product_thumbnail_width');
                        $max_product_thumbnail_height = $tagalysConfiguration->getTagalysConfig('max_product_thumbnail_height');
                        $imageObj = new Varien_Image($thisProductImagePath);
                        $imageObj->keepTransparency(true);
                        $imageObj->constrainOnly(TRUE);
                        $imageObj->keepAspectRatio(TRUE);
                        $imageObj->keepFrame(FALSE);
                        $imageObj->resize($max_product_thumbnail_width, $max_product_thumbnail_height);
                        $imageObj->save($resizedProductImagePath);
                    }
                    if (file_exists($resizedProductImagePath)) {
                        return str_replace('http:', '', Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'tagalys' . DS . 'product_thumbnails' . $productImagePath);
                    }
                }
            }
        } catch(Exception $e) {
            Mage::log("Exception in getProductImageUrl: {$e->getMessage()}", null, 'tagalys-image-generation.log');
        }
        // placeholder
        $placeholder_image = Mage::getStoreConfig("catalog/placeholder/{$image_attribute_code}_placeholder");
        $media_base_url = Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $media_url = $media_base_url . Mage::getModel('catalog/product_media_config')->getBaseMediaUrlAddition();
        return false;
    }
}