<?php

class Tagalys_Core_Model_Queue extends Mage_Core_Model_Abstract {
    protected function _construct(){
        $this->_init("tagalys_core/queue");
    }

    public function queuePrimaryProductIdFor($product_id, $store_id = null) {
        $primary_product_id = $this->getPrimaryProductId($product_id);
        if ($primary_product_id === false) {
            // no related product id
        } elseif ($product_id == $primary_product_id) {
            // same product. so no related product id.
        } else {
            if ($store_id == null) {
                $this->blindlyAddProduct($primary_product_id);
            } else {
                $this->blindlyAddProductForStore($primary_product_id, $store_id);
            }
        }
        return $primary_product_id;
    }

    public function prune($limit = 100) {
        // remove products with visibility 1 (not visible invidivually); replace with the configurable product if exists
        $changed_count = 0;
        $queue_collection = $this->getCollection()->setOrder('id', 'ASC')->setPageSize($limit);
        foreach ($queue_collection as $i => $queue_item) {
            $product_id = $queue_item->getData('product_id');
            $primary_product_id = $this->queuePrimaryProductIdFor($product_id);
            if ($primary_product_id == false || $product_id != $primary_product_id) {
                $queue_item->delete();
            }
        }
        if ($changed_count > 0) {
            prune($limit);
        }
    }

    public function _visibleInAnyStore($product_id) {
        $visible = false;
        $store_ids = Mage::helper("tagalys_core")->getStoresForTagalys();
        foreach ($store_ids as $store_id) {
            Mage::app()->setCurrentStore($store_id);
            $product = Mage::getModel('catalog/product')->load($product_id);
            $product_visibility = $product->getVisibility();
            if ($product_visibility != 1) {
                $visible = true;
                break;
            }
        }
        return $visible;
    }

    public function getPrimaryProductId($product_id) {
        $product = Mage::getModel('catalog/product')->load($product_id);
        if ($product) {
            $product_type = $product->getTypeId();
            $visible_in_any_store = $this->_visibleInAnyStore($product_id);
            if (!$visible_in_any_store) {
                // not visible individually
                if ($product_type == 'simple') {
                    // coulbe be attached to configurable product
                    $parent_ids = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product_id);
                    if (count($parent_ids) > 0) {
                        // check and return configurable product id
                        return $this->getPrimaryProductId($parent_ids[0]);
                    }
                } else {
                    // configurable / grouped / bundled product that is not visible individually
                    return false;
                }
            } else {
                // any type of product that is visible individually. add to queue.
                return $product_id;
            }
        } else {
            // product not found. might have to delete
            return $product_id;
        }
    }

    public function blindlyAddProduct($product_id) {
        $store_ids = Mage::helper("tagalys_core")->getStoresForTagalys();
        foreach ($store_ids as $store_id) {
            $this->blindlyAddProductForStore($product_id, $store_id);
        }
    }

    public function blindlyAddProductForStore($product_id, $store_id) {
        $collection = $this->getCollection()
            ->addFieldToFilter('product_id', $product_id)
            ->addFieldToFilter('store_id', $store_id)
            ->setPageSize(1);

        if ($collection->getSize()) {
            // already exists
            return false;
        } else {
            $data = array('product_id' => $product_id, 'store_id' => $store_id);
            $this->setData($data);
            try {
                $this->save();
                return true;
            } catch(Exception $e) {
                Mage::log("Error adding product_id $product_id to tagalys_queue for store_id $store_id.", null, "tagalys_core.log");
            }
            return false;
        }
    }

    public function migrateUpdatesQueueIfRequired() {
        $store_ids = Mage::helper("tagalys_core")->getStoresForTagalys();
        $sql = "SELECT * FROM {$this->getTableName()} WHERE store_id IS NULL;";
        $rows = $this->runSqlRead($sql);
        if(count($rows) == 0) {
            return false;
        }
        $validRows = [];
        foreach($store_ids as $store_id) {
            foreach ($rows as $row) {
                $row['store_id'] = $store_id;
                $validRows[] = "({$row['product_id']}, {$row['store_id']})";
            }
        }
        $this->paginateAndInsertRows($validRows);

        $sql = "DELETE FROM {$this->getTableName()} WHERE store_id IS NULL;";
        $this->runSqlWrite($sql);

        return true;
    }

    private function getTableName() {
        $resource = Mage::getSingleton('core/resource');
        return $resource->getTableName('tagalys_core/queue');
    }

    private function runSqlRead($sql){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        return $readConnection->fetchAll($sql);
    }
    private function runSqlWrite($sql){
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->query($sql);
    }

    public function paginateAndInsertRows($rows) {
        $queueTable = $this->getTableName();
        $sqlBulkBatchSize = 10;
        Mage::helper('tagalys_core')->forEachChunk($rows, $sqlBulkBatchSize, function($rowsToInsert) use ($queueTable){
            $values = implode(',', $rowsToInsert);
            $sql = "REPLACE $queueTable (product_id, store_id) VALUES $values;";
            $this->runSqlWrite($sql);
        });
    }

    public function getUpdatesCount($store_id = null) {
        $collection = $this->getCollection();
        if ($store_id != null) {
            $collection->addFieldToFilter('store_id', $store_id);
        }
        return $collection->getSize();
    }

    public function deleteItemsByStoreId($store_id) {
        $sql = "DELETE FROM {$this->getTableName()} WHERE store_id = $store_id;";
        $this->runSqlWrite($sql);
    }

    public function getProductIdsForStore($store_id = null, $batch_size = 500) {
        $collection = $this->getCollection();
        if ($store_id != null) {
            $collection->addFieldToFilter('store_id', $store_id);
        }
        $collection->addFieldToSelect('product_id');
        $collection->setOrder('id', 'ASC');
        $collection->setPageSize($batch_size);
        $product_ids = [];
        foreach ($collection as $item) {
            $product_ids[] = $item->getData('product_id');
        }
        return $product_ids;
    }

    public function paginateSqlDelete($product_ids, $stores = null) {
        $sqlBulkBatchSize = 1000;
        Mage::helper('tagalys_core')->forEachChunk($product_ids, $sqlBulkBatchSize, function($idsChunk) use($stores) {
            $values = implode(',', $idsChunk);
            $where = "product_id IN ($values)";
            if ($stores != null) {
                $where .= " AND store_id IN (" . implode(',', $stores) . ")";
            }
            $sql = "DELETE FROM {$this->getTableName()} WHERE $where;";
            $this->runSqlWrite($sql);
        });
        return true;
    }
}
