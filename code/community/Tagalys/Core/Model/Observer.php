<?php
class Tagalys_Core_Model_Observer extends Varien_Object {
    public function categorySaved(Varien_Event_Observer $observer) {
        try {
            $category = $observer->getEvent()->getCategory();
            $this->markPendingSync($category->getId());
            $products = $category->getPostedProducts();
            $oldProducts = $category->getProductsPosition();
            $insert = array_diff_key($products, $oldProducts);
            $delete = array_diff_key($oldProducts, $products);
            foreach($insert as $productId => $pos) {
                Mage::getModel('tagalys_core/queue')->blindlyAddProduct($productId);
            }
            foreach($delete as $productId => $pos) {
                Mage::getModel('tagalys_core/queue')->blindlyAddProduct($productId);
            }
        } catch (Exception $e) {
            Mage::log("Exception on categorySaved: " . $e->getMessage(), null, "tagalys_core.log");
        }
    }
    public function tagalys_distpatch(Varien_Event_Observer $observer) { 
        try {
            $params = $observer->getEvent()->getControllerAction()->getRequest()->getParams();
            $tagalys_config_events = array('adminhtml_catalog_product_attribute_delete','adminhtml_catalog_product_attribute_save', 'adminhtml_system_currency_saveRates','adminhtml_system_currencysymbol_save');
            if (in_array ($observer->getEvent()->getControllerAction()->getFullActionName(), $tagalys_config_events)) {
                // sync config
                Mage::getModel('tagalys_core/config')->setTagalysConfig("config_sync_required", '1');
            }
            if (in_array ($observer->getEvent()->getControllerAction()->getFullActionName(), array("adminhtml_catalog_category_save"))) {
                $stores = Mage::helper('tagalys_core')->getStoresForTagalys();
                foreach($stores as $i => $store_id) {
                    Mage::getModel('tagalys_core/config')->setTagalysConfig("store:{$store_id}:resync_required", 1);
                }
            }
            if (isset($params) && isset($params['section']) && $params["section"] == "currency") {
                // sync config
                Mage::getModel('tagalys_core/config')->setTagalysConfig("config_sync_required", 1);
            }
        } catch (Exception $e) {
            Mage::log("Error in tagalys_distpatch: ". $e->getMessage(), null, "tagalys_core.log");
        }
    }

    // create / update / delete from admin
    public function productChanged(Varien_Event_Observer $observer) {
        try {
            $product = $observer->getEvent()->getProduct();
            $product_id = $product->getId();
            Mage::getModel('tagalys_core/queue')->blindlyAddProduct($product_id);
        } catch (Exception $e) {
            Mage::log("Exception on productChanged: " . $e->getMessage(), null, "tagalys_core.log");
        }
    }

    // csv import
    public function productsImported(Varien_Event_Observer $observer) {
        try {
            $adapter = $observer->getEvent()->getAdapter();
            $affectedEntityIds = $adapter->getAffectedEntityIds();
            foreach($affectedEntityIds as $product_id) {
                Mage::getModel('tagalys_core/queue')->blindlyAddProduct($product_id);
            }
        } catch (Exception $e) {
            Mage::log("Exception on productsImported: " . $e->getMessage(), null, "tagalys_core.log");
        }
    }

    // multiple products checkbox -> action
    public function productsAttributesChanged(Varien_Event_Observer $observer) {
        try {
            $updatedProductIds = $observer->getEvent()->product_ids;
            foreach($updatedProductIds as $product_id) {
                Mage::getModel('tagalys_core/queue')->blindlyAddProduct($product_id);
            }
        } catch (Exception $e) {
            Mage::log("Exception on productsAttributesChanged: " . $e->getMessage(), null, "tagalys_core.log");
        }
    }

    private function markPendingSync($categoryId){
        $categories = Mage::getModel('tagalys_core/categories')->getCollection()->addFieldToFilter('category_id', $categoryId);
        foreach($categories as $category){
            if($category->getStatus()== 'powered_by_tagalys'){
                $category->setStatus('pending_sync')->save();
            }
        }
    }
}