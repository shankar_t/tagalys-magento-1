<?php
class Tagalys_Core_Model_Categories extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    parent::_construct();
    $this->_init("tagalys_core/categories");
  }

  public function createOrUpdateWithData($storeId, $categoryId, $createData, $updateData)
  {
    $firstItem = $this->getCollection()
      ->addFieldToFilter('category_id', $categoryId)
      ->addFieldToFilter('store_id', $storeId)
      ->getFirstItem();

    try {
      if ($id = $firstItem->getId()) {
        $updateData['category_id'] = $categoryId;
        $updateData['store_id'] = $storeId;
        $model = $this->load($id)->addData($updateData);
        $model->setId($id)->save();
      } else {
        $createData['category_id'] = $categoryId;
        $createData['store_id'] = $storeId;
        $model = Mage::getModel("tagalys_core/categories")->setData($createData);
        $insertId = $model->save()->getId();
      }
    } catch (Exception $e) {
      Mage::getSingleton('tagalys_core/client')->log('error', 'Exception in setUpdateRequiredForCategory', array('exception_message' => $e->getMessage()));
    }
  }

  public function updateWithData($storeId, $categoryId, $data)
  {
    $data['category_id'] = $categoryId;
    $data['store_id'] = $storeId;

    $firstItem = $this->getCollection()
      ->addFieldToFilter('category_id', $categoryId)
      ->addFieldToFilter('store_id', $storeId)
      ->getFirstItem();

    try {
      if ($id = $firstItem->getId()) {
        $model = $this->load($id)->addData($data);
        $model->setId($id)->save();
      }
    } catch (Exception $e) {
      Mage::getSingleton('tagalys_core/client')->log('error', 'Exception in updateWithData', array('exception_message' => $e->getMessage()));
    }
  }
  
  public function markStoreCategoryIdsForDeletionExcept($storeId, $categoryIds)
  {
    $collection = $this->getCollection()->addFieldToFilter('store_id', $storeId);
    foreach ($collection as $collectionItem) {
      if (!in_array((int)$collectionItem->getCategoryId(), $categoryIds)) {
        $collectionItem->addData(array('marked_for_deletion' => 1))->save();
      }
    }
  }
  
}
