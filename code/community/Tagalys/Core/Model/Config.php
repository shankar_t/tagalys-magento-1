<?php
class Tagalys_Core_Model_Config extends Mage_Core_Model_Abstract {
    public function _construct() {
        parent::_construct();
        $this->_init("tagalys_core/config");
    }

    public function checkStatusCompleted() {
        $setup_status = $this->getTagalysConfig('setup_status');
        if ($setup_status == 'sync') {
            $store_ids = Mage::helper("tagalys_core")->getStoresForTagalys();
            $all_stores_completed = true;
            foreach($store_ids as $store_id) {
                $store_setup_status = $this->getTagalysConfig("store:$store_id:setup_complete");
                if ($store_setup_status != '1') {
                    $all_stores_completed = false;
                    break;
                }
            }
            if ($all_stores_completed) {
                $this->setTagalysConfig("setup_status", 'completed');
                $package_name = Mage::getStoreConfig('tagalys/package/name');
                $modules_to_activate = array();
                switch($package_name) {
                    case 'Search':
                        $modules_to_activate = array('search');
                        break;
                    case 'Mpages':
                        $modules_to_activate = array('mpages');
                        break;
                }
                Mage::getSingleton('tagalys_core/client')->log('info', 'All stores synced. Enabling Tagalys features.', array('modules_to_activate' => $modules_to_activate));
                foreach($modules_to_activate as $module_to_activate) {
                    $this->setTagalysConfig("module:$module_to_activate:enabled", '1');
                }
            }
        }
    }

    public function getTagalysConfig($config, $json_decode = false) {
        $configValue = $this->getCollection()->addFieldToFilter('path',$config)->getFirstItem()->getData("value");
        if ($configValue === NULL) {
            $defaultConfigValues = array(
                'tagalys:health' => '1',
                'setup_status' => 'api_credentials',
                'periodic_full_sync' => '1',
                'search_box_selector' => '#search',
                'max_product_thumbnail_width' => '400',
                'max_product_thumbnail_height' => '400',
                'cron_heartbeat_sent' => false,
                'suggestions_align_to_parent_selector' => '',
                'module:mpages:enabled' => '1',
                'categories'=> '[]',
                'category_ids'=> '[]',
                'listing_pages:override_layout'=> '1',
                'listing_pages:override_layout_name'=> 'one_column',
                'listing_pages:position_sort_direction' => 'asc',
                'product_image_attribute' => 'small_image',
                'product_image_hover_attribute' => '',
                'category_pages_rendering_method' => 'magento',
                'category_pages_store_mapping' => '{}',
                'tagalys_plan_features' => '{}',
                'legacy_mpage_categories' => '[]',
                'sync_file_ext' => 'jsonl',
                'success_order_states' => '["new", "payment_review", "processing", "complete", "closed"]',
                'js_rendered_stores' => '[]',
                'sync:whitelisted_attributes' => '[]',
                'sync:get_min_max_prices_for_configurable_products' => 'false'
            );
            if (array_key_exists($config, $defaultConfigValues)) {
                $configValue = $defaultConfigValues[$config];
            } else {
                $configValue = NULL;
            }
        }
        if ($configValue !== NULL && $json_decode) {
            return json_decode($configValue, true);
        }
        return $configValue;
    }

    public function getTagalysPlanFeature($feature) {
      $features = $this->getTagalysConfig('tagalys_plan_features', true);
      return $features[$feature];
    }

    public function setTagalysConfig($config, $value, $json_encode = false) {
        if ($json_encode) {
            $value = json_encode($value);
        }
        $data = array('path' => $config, 'value' => $value);

        $collection = $this->getCollection()->addFieldToFilter('path',$config)->getFirstItem();

        try {
            if ($id = $collection->getId()){
                $model = $this->load($id)->addData($data);
                $model->setId($id)->save();
            } else {
                $model = Mage::getModel("tagalys_core/config")->setData($data);
                $insertId = $model->save()->getId();
            }
        } catch (Exception $e){
            Mage::getSingleton('tagalys_core/client')->log('error', 'Exception in setTagalysConfig', array('exception_message' => $e->getMessage()));
        }
    }

    public function getStoresForTagalys() {
        $storesForTagalys = $this->getTagalysConfig("stores", true);
        
        if ($storesForTagalys != NULL) {
            if (!is_array($storesForTagalys)) {
                $storesForTagalys = array($storesForTagalys);
            }
            return $storesForTagalys;
        }
        return array();
    }

    public function getCategoriesForTagalys($storeId) {
        $categoriesForTagalys = Mage::getModel("tagalys_core/categories")
            ->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('marked_for_deletion', false)
            ->addFieldToSelect('category_id');
        $response = array();
        foreach($categoriesForTagalys as $category){
            $category = Mage::getModel('catalog/category')->load($category->get('category_id')['category_id']);
            // $category->getPath();
            array_push($response, $category->getPath());
        }
        return $response;
    }

    public function getAllCategories($storeId) {
        $currentStore = Mage::app()->getStore();
        Mage::app()->setCurrentStore(Mage::app()->getStore($storeId));
        $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->setStoreId($storeId)
            ->addFieldToFilter('is_active', 1)
            ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
            ->addAttributeToSelect('*');

        foreach ($categories as $category) {
            $pathIds = explode('/', $category->getPath());
            if (count($pathIds) > 2) {
                $label = $this->getCategoryName($category);
                $output[] = array('value' => implode('/', $pathIds), 'label' => $label, 'static_block_only' => ($category->getDisplayMode() == Mage_Catalog_Model_Category::DM_PAGE));
            }
        }
        Mage::app()->setCurrentStore($currentStore);
        return $output;
    }

    public function getCategoryName($category) {
        $unorderedPathNames = array();
        $pathNames = array();
        $pathIds = explode('/', $category->getPath());
        if (count($pathIds) > 2) {
            $path = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('entity_id', array('in' => $pathIds));
            foreach($path as $i => $path_category) {
              if ($i != 1) { // skip root category
                $unorderedPathNames[$path_category->getId()] = $path_category->getName();
              }
            }
            foreach($pathIds as $id){
                if($id != 1){
                    $pathNames[] = $unorderedPathNames[$id];
                }
            }
        }
        return implode(' |>| ', $pathNames);
    }

    public function getCategoryTreeData($storeId){
      $flat_category_list = $this->getAllCategories($storeId);
      $selected_categories = $this->getCategoriesForTagalys($storeId);
      $tree = array();
      $tagalysCategoryHelper = Mage::helper('tagalys_core/categories');
      $tagalysCreatedCategories = $tagalysCategoryHelper->getTagalysCreatedCategories();
      foreach ($flat_category_list as $category){
        $category_id_path = explode('/',$category['value']);
        $category_label_path = explode(' |>| ',$category['label']);
        if($category_id_path[0] == 1){
          array_splice($category_id_path, 0, 1);
        }
        if (in_array(end($category_id_path), $tagalysCreatedCategories)) {
          continue;
        } else {
          foreach($selected_categories as $selected_category){
            if ($selected_category == $category['value']) {
              $category['selected'] = true;
              $selected_category = end(explode('/', $selected_category));
              $category_sync_status = Mage::getModel("tagalys_core/categories")
                ->getCollection()
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('category_id', end($category_id_path))
                ->addFieldToSelect(array('status', 'positions_synced_at'))
                ->getFirstItem();
              $category['status'] = $category_sync_status['status'];
              $category['positions_synced_at'] = $category_sync_status['positions_synced_at'];
            }
          }
        }
        $tree = $this->constructTree($category_id_path, $category_label_path, $tree, $category);
      }
      return json_encode($tree);
    }

    private function constructTree($category_id_path, $category_label_path, $children, $category_object){
      if(count($category_id_path)==1){
        // Append to array
        $node_exist = false;
        for($i=0;$i<count($children);$i++){
          if($children[$i]['id']==$category_id_path[0]){
            $node_exist = true;
            $children[$i]['value'] = $category_object['value'];
            $children[$i]['state'] = array();
            $children[$i]['state']['disabled'] = $category_object['static_block_only'];
            if($category_object['selected']==true){
              $children[$i]['state']['selected'] = true;
              $iconAndText = $this->getCategoryStatusIconAndText($category_object);
              $children[$i]['icon'] = $iconAndText['icon'];
              if ($iconAndText['icon'] != 'hidden') {
                $children[$i]['text'] .= $iconAndText['text'];
              }
            }
          }
        }
        if(!$node_exist){
          $node = array(
            'id'=>$category_id_path[0], 
            'value'=>$category_object['value'],
            'text'=>$category_label_path[0].($category_object['static_block_only'] ? ' (Static block only)' : ''), 
            'state'=> array('selected'=> (array_key_exists('selected', $category_object) && $category_object['selected']==true) ? true : false, 'disabled' => $category_object['static_block_only']),
            'children'=>array(),
            'icon' => $this->getCategoryStatusIconAndText($category_object)
          );
          $iconAndText = $this->getCategoryStatusIconAndText($category_object);
          $node['icon'] = $iconAndText['icon'];
          if ($iconAndText['icon'] != 'hidden') {
              $node['text'] .= $iconAndText['text'];
          }
          $children[] = $node;
        }
      } else {
        // Find the parent to pass to
        $child_exist = false;
        for($i=0;$i<count($children);$i++){
          if($children[$i]['id']==$category_id_path[0]){
            $child_exist = true;
            $children[$i]['children']=$this->constructTree(
              array_slice($category_id_path, 1), 
              array_slice($category_label_path, 1), 
              $children[$i]['children'], 
              $category_object
            );
            break;
          }
        }
        if(!$child_exist){
          // Create the parent
          $children[]=array(
            'id'=>$category_id_path[0], 
            'value'=> 'NOT_AVAILABLE',
            'text' => $category_label_path[0].($category_object['static_block_only'] ? ' (Static block only)' : ''),
            'state' => array('disabled' => true, 'opened' => true), // Only for ROOT (eg. defautl category) categories
            'children' => $this->constructTree(array_slice($category_id_path, 1), array_slice($category_label_path, 1), array(), $category_object),
            'icon' => 'hidden'
          );
        }
      }
      return $children;
    }

    private function getCategoryStatusIconAndText($categoryObject){
      if (!array_key_exists('status', $categoryObject)) {
        $categoryObject['status'] = '';
      }
      switch($categoryObject['status']){
        case 'pending_sync':
          return array('icon' => 'fa fa-refresh', 'text' => ' (Sync Pending)');
        case 'failed':
          return array('icon' => 'fa fa-exclamation-triangle', 'text' => ' (No Products Found)');
        case 'powered_by_tagalys':
          if ($categoryObject['positions_synced_at'] == NULL) {
            return array('icon' => 'fa fa-refresh', 'text' => ' (Sync Pending)');
          } else {
            return array('icon' => 'fa fa-bolt', 'text' => ' (Powered by Tagalys)');
          }
        default:
          return array('icon' => 'hidden', 'text' => '');
      }
    }

    public function isSortedReverse(){
        return $this->getTagalysConfig('listing_pages:position_sort_direction') != 'asc';
    }
}