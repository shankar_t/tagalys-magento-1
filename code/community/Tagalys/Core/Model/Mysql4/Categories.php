<?php
class Tagalys_Core_Model_Mysql4_Categories extends Mage_Core_Model_Mysql4_Abstract {
    protected function _construct() {
        $this->_init("tagalys_core/categories", "id");
    }
    public function truncate() {
        $this->_getWriteAdapter()->query('TRUNCATE TABLE '.$this->getMainTable());
    }
}