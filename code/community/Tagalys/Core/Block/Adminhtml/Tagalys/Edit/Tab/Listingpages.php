<?php

class Tagalys_Core_Block_Adminhtml_Tagalys_Edit_Tab_Listingpages extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    public function __construct() {
        parent::__construct();
    }

    protected function _prepareForm() {
        $this->_helper = Mage::helper('tagalys_core');

        $form = Mage::getModel('varien/data_form', array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/tagalys', array('_current'  => true)),
            'method'  => 'post'
        ));

        $form->setHtmlIdPrefix('admin_tagalys_core_');
        $htmlIdPrefix = $form->getHtmlIdPrefix();

        $enableFieldset = $form->addFieldset(
            'enable_listingpages_fieldset',
            ['legend' => $this->__('Enable Listing Pages')]
        );
        $enableFieldset->addField('enable_listingpages', 'select', array(
            'name' => 'enable_listingpages',
            'label' => 'Use Tagalys to power Category pages',
            'title' => 'Use Tagalys to power Category pages',
            'options' => array(
                '0' => $this->__('No'),
                '1' => $this->__('Yes - For selected category pages')
            ),
            'required' => false,
            'style' => 'width:100%',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("module:listingpages:enabled")
        ));

        $technicalConsiderationsFieldset = $form->addFieldset(
            'technical_considerations_fieldset',
            ['legend' => $this->__('Technical Considerations'), 'collapseable' => true]
        );

        $renderingMethod = Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:rendering_method");
        if($renderingMethod=='tagalys_js'){
            $technicalConsiderationsFieldset->addField('category_pages_rendering_method', 'select', array(
                'name' => 'category_pages_rendering_method',
                'label' => 'Render category pages UI using',
                'title' => 'Render category pages UI using',
                'options' => array(
                    'platform' => $this->__('Magento (recommended)'),
                    'tagalys_js' => $this->__('Tagalys JavaScript')
                ),
                'required' => false,
                'style' => 'width:100%',
                'value' => $renderingMethod,
                'after_element_html' => '<span style="display: block; margin-top:5px">Not sure how to decide? <a href="https://tagalys.freshdesk.com/support/solutions/articles/26000037028-powering-magento-category-pages-with-tagalys/preview" target="_blank">Read this.</a></span>',
            ));
        }

        // magento

        $multiStoreWarningRequired = Mage::helper('tagalys_core/categories')->isMultiStoreWarningRequired();

        $magentoNote = '<p>Tagalys operates by updating Magento product positions for the categories selected below. Your current values will be overwritten.</p>';
        $magentoNote .= '<p><strong>Important:</strong> To make sure it works as expected, please verify the following:</p>';
        $magentoNote .= "<ul>
            <li>Your category pages are configured to be sorted by the Position field.</li>
            <li>Your category pages sorting direction matches your choice below. This could be set in one of your blocks / view files, so please double-check.</li>
            <li>If you have any custom / front-end caches enabled, you may have to listen to Tagalys events and clear cache as appropriate. Tagalys fires the event tagalys_category_positions_updated with the category ID that was updated in the payload so you can do this. If you have any questions on this, please contact us.</li>
            <li>Your Cron is setup correctly as described in the Sync Settings tab</li>
        </ul>";
        if ($multiStoreWarningRequired) {
            $magentoNote .= "
                <p><strong>Important:</strong> You have multiple stores with common categories. Since Magento does not allow specifying separate product positions per store, please review the following:</p>
                <ul>
                    <li>If all or most of your products are common across all your stores, choose a single store below. Your category pages will be listed under this store on the Tagalys Dashboard and positions will be updated based on that store, but will apply to all your stores.</li>
                    <li>If you have different products across stores or have a more complex setup, please contact us on how best to choose categories below.</li>
                </ul>
            ";
        }
        $technicalConsiderationsFieldset->addField('rendering_method_note_platform', 'note', array(
            'label' => $this->__('Notes for the tech team'),
            'text' => '<div class="tagalys-note visibile-for-rendering-method visibile-for-rendering-method-platform">'.$magentoNote.'</div>'
        ));

        if ($multiStoreWarningRequired) {
            $technicalConsiderationsFieldset->addField('same_or_similar_products_across_all_stores', 'select',  array(
                'label' => 'Are all or most of your products common across all stores?',
                'name' => 'same_or_similar_products_across_all_stores',
                'options' => array(
                    '1' => $this->__('Yes, all or most of our products are common across all stores'),
                    '0' => $this->__('No, we have different products across stores')
                ),
                'required' => false,
                'style' => 'width:100%',
                'class' => 'visibile-for-rendering-method visibile-for-rendering-method-platform',
                'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:same_or_similar_products_across_all_stores")
            ));

            $storeOptions = array();
            foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $key => $storeId) {
                $store = Mage::getModel('core/store')->load($storeId);
                $group = $store->getGroup();
                $website = $group->getWebsite();
                $storeDisplayLabel = $website->getName() . ' / '. $group->getName() . ' / ' . $store->getName();
                $storeOptions[$storeId] = $storeDisplayLabel;
            }
            
            $technicalConsiderationsFieldset->addField('store_id_for_category_pages', 'select',  array(
                'label' => 'Which store do you want to use to sync Category pages to Tagalys?',
                'name' => 'store_id_for_category_pages',
                'options' => $storeOptions,
                'required' => false,
                'style' => 'width:100%',
                'class' => 'visibile-for-rendering-method visibile-for-rendering-method-platform visible-for-same-products-across-stores visible-for-same-products-across-stores-1',
                'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig( "listing_pages:store_id_for_category_pages")
            ));
        }

        $technicalConsiderationsFieldset->addField('position_sort_direction', 'select',  array(
            'label' => 'What direction of the Position field are category pages sorted by?',
            'name' => 'position_sort_direction',
            'options' => array(
                'asc' => 'Ascending (default)',
                'desc' => 'Descending'
            ),
            'required' => false,
            'style' => 'width:100%',
            'class' => 'visibile-for-rendering-method visibile-for-rendering-method-platform',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:position_sort_direction")
        ));

        // common

        $technicalConsiderationsFieldset->addField('understand_and_agree', 'text', array(
            'name'      => 'understand_and_agree',
            'label'     => 'Do you understand the above settings and agree to allow Tagalys to power category pages? Please verify with your tech team.',
            'value'     => Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:understand_and_agree"),
            'required'  => false,
            'style'     => "width:100%",
            'after_element_html' => '<p><small style="font-weight: bold">Contact support@tagalys.com if you have any questions. <span style="color: #aa0000">Type "I agree" above to choose categories.</span></small></p>',
            'tabindex' => 1
        ));

        $jsRenderedStores = $form->addFieldset(
            'js_rendered_stores_fieldset',
            ['legend' => $this->__('JS Rendered Categories'), 'collapseable' => true]
        );
        $jsRenderedStores->addField('js_rendered_stores', 'multiselect', array(
            'name'      => 'js_rendered_stores',
            'style' => "width:100%; height: 125px; display:none;",
            'onclick' => "return false;",
            'onchange' => "return false;",
            'value'  => $this->_helper->getJsRenderedStores(),
            'values' => $this->_helper->getTagalysStoresForSelect(),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));
        $jsRenderedStoreTreeData = htmlspecialchars($this->_helper->getJsRenderedStoreTreeData(), ENT_QUOTES, 'UTF-8');
        $jsRenderedStores->addField('js_rendered_stores_warp', 'note', array(
            'label' => $this->__('Choose stores for which you want to enable Tagalys JS rendering'),
            'text' => "<input id='js-rendered-stores-jtree-q' /><div id='js-rendered-stores-jtree' data-tree=\"{$jsRenderedStoreTreeData}\" ></div>"
        ));

        $jsRenderedStores->addField('js_rendering_note', 'note', array(
            'label' => $this->__('Notes for the tech team on JS rendering'),
            'text' => '<div class="tagalys-note">
                <ul class="">
                    <li>Tagalys JS renders its own HTML and UI customization has to be done to make it match your theme</li>
                    <li>Tagalys will replace the template used to render the <em>category.products</em> block, so make sure that this is present in your layout</li>
                    <li>Since Tagalys renders filters and products within this block, recommended settings are to override the layout and use <em>1column</em></li>
                    <li>If you have some custom UI rules common to all Tagalys pages, then you could create a separate custom layout and override that instead of <em>1column</em></li>
                    <li>If you need control over each page, avoid overriding the layout and use Magento controls under Catalog&nbsp;>&nbsp;Category for each page to specify the layout and any updates you need</li>
                    <li>You may have to clear your Magento cache after updating these settings</li>
                    <li>Please contact cs@tagalys.com if you have any questions</li>
                </ul>
            </div>'
        ));

        $jsRenderedStores->addField('override_layout_for_listing_pages', 'select', array(
            'name' => 'override_layout_for_listing_pages',
            'label' => 'Override layout for Tagalys powered category pages',
            'title' => 'Override layout for Tagalys powered category pages',
            'options' => array(
                '0' => $this->__('No'),
                '1' => $this->__('Yes')
            ),
            'required' => false,
            'style' => 'width:100%',
            'class' => '',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:override_layout")
        ));

        $jsRenderedStores->addField('override_layout_name_for_listing_pages', 'text', array(
            'name'      => 'override_layout_name_for_listing_pages',
            'label'     => $this->__('Layout name to override with'),
            'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:override_layout_name"),
            'required'  => true,
            'style'   => "width:100%",
            'class' => '',
            'tabindex' => 1
        ));

        $storeMapping = Mage::getModel('tagalys_core/config')->getTagalysConfig("category_pages_store_mapping", true);
        foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $key => $storeId) {
            $hidden = false;
            if (array_key_exists($storeId . '', $storeMapping) && $storeMapping[$storeId.''] != $storeId.'') {
                $hidden = true;
            }
            $store = Mage::getModel('core/store')->load($storeId);
            $group = $store->getGroup();
            $website = $group->getWebsite();
            $storeDisplayLabel = $website->getName() . ' / '. $group->getName() . ' / ' . $store->getName();
            $smartPageParentCategoryId = Mage::getModel('tagalys_core/config')->getTagalysConfig("tagalys_parent_category_store_$storeId", true);
            $smartPageParentCategory = Mage::getModel('catalog/category')->load($smartPageParentCategoryId);
            if($smartPageParentCategory->getId()){
                $name = $smartPageParentCategory->getName();
                $urlKey = $smartPageParentCategory->getUrlKey();
                $smartPageEnabled = true;
            } else {
                $smartPageEnabled = false;
                $name = 'Tagalys';
                $urlKey = 'buy';
            }
            $storeListingPagesFieldset = $form->addFieldset(
                'store_'.$storeId.'_listing_pages',
                ['legend' => 'Categories for store: '.$storeDisplayLabel, 'collapsable' => true]
            );

            $storeListingPagesFieldset->addField("enable_smart_page_store_$storeId", 'select', array(
                'name' => "enable_smart_page_store_$storeId",
                'label' => "Enable Smart Categories for this store",
                'options' => array(
                    '1' => $this->__('Yes (Recommended)'),
                    '0' => $this->__('No')
                ),
                'after_element_html' => '<p><small style="font-weight: bold">This will allow you to create new categories from the Tagalys Dashboard whose products are dynamically managed by Tagalys based on various conditions.</small></p>',
                'data-store-id' => 3,
                'style' => 'width:100%',
                'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("enable_smart_page_store_$storeId"),
            ));
            $storeListingPagesFieldset->addField("smart_page_parent_category_name_store_$storeId", 'text', array(
                'name' => "smart_page_parent_category_name_store_$storeId",
                'label' => "Tagalys parent category name",
                'after_element_html' => '<p><small style="font-weight: bold">For your reference, not shown in the front-end.</small></p>',
                'value'  => $name,
                'style' => 'width:100%',
            ));
            $storeListingPagesFieldset->addField("smart_page_parent_category_url_key_store_$storeId", 'text', array(
                'name' => "smart_page_parent_category_url_key_store_$storeId",
                'label' => "Tagalys parent category url key",
                'value'  => $urlKey,
                'style' => 'width:100%',
                'disabled' => $smartPageEnabled
            ));

            $storeListingPagesFieldset->addField('categories_for_tagalys_store_'.$storeId, 'multiselect', array(
                'name'      => 'categories_for_tagalys_store_'.$storeId,
                'onclick' => "return false;",
                'onchange' => "return false;",
                'value'  => Mage::getModel('tagalys_core/config')->getCategoriesForTagalys($storeId),
                'values' => Mage::getModel('tagalys_core/config')->getAllCategories($storeId),
                'style' => "width:100%; height: 400px; display: none;",
                'class' => 'categories-for-tagalys-store' . ($hidden ? ' force-hide-store-categories' : ''),
                'disabled' => false,
                'readonly' => false,
                'tabindex' => 1
            ));
            $category_tree_data = htmlspecialchars( Mage::getModel('tagalys_core/config')->getCategoryTreeData($storeId), ENT_QUOTES, 'UTF-8');
            $storeListingPagesFieldset->addField('jtree_wrap_store_'.$storeId, 'note', array(
                'label' => '',
                'text'=>"<input id='categories-jtree-store-{$storeId}-q' /><button style='margin-left: 10px' id='select-all-category-store-{$storeId}' class='tagalys-btn'>Select all</button><button style='margin-left: 10px' id='deselect-all-category-store-{$storeId}' class='tagalys-btn'>Deselect all</button><div id='categories-jtree-store-{$storeId}' data-tree=\"{$category_tree_data}\" ></div>"
            ));
        }

        $submitFieldset = $form->addFieldset(
            'save_listingpages',
            ['legend' => $this->__('Save Changes')]
        );
        $submitFieldset->addField('save-delay-note', 'note', array(
            'label' => '',
            'text' => '<div class="tagalys-note">Once saved, selected categories will be visible in your Tagalys Dashboard within 10 minutes and product positions on these categories will be updated within 15 minutes unless otherwise mentioned.</div>'
        ));
        $submitFieldset->addField('submit', 'submit', array(
            'name' => 'tagalys_submit_action',
            'value' => 'Save Listing Pages Settings',
            'class'=> "tagalys-btn"
        ));

        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Tab label getter
     *
     * @return string
     */
    public function getTabLabel() {
        return $this->__('Listing Pages');
    }

    /**
     * Tab title getter
     *
     * @return string
     */
    public function getTabTitle() {
        return $this->__('Listing Pages');
    }

    /**
     * Check if tab can be shown
     *
     * @return bool
     */
    public function canShowTab() {
        return true;
    }

    /**
     * Check if tab hidden
     *
     * @return bool
     */
    public function isHidden() {
        return false;
    }

}