<?php

class Tagalys_Core_Block_Adminhtml_Tagalys_Edit_Tab_Syncsettings extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {
    
    public function __construct() {
        parent::__construct();
    }
    
    protected function _prepareForm() {
        $this->_helper = Mage::helper('tagalys_core');

        $form = Mage::getModel('varien/data_form', array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/tagalys', array('_current'  => true)),
            'method'  => 'post'
        ));

        $form->setHtmlIdPrefix('tagalys_admin_core_');
        $htmlIdPrefix = $form->getHtmlIdPrefix();

        $cron_fieldset = $form->addFieldset('tagalys_cron_fieldset', array('legend' => $this->__('Cron Setup')));
        $cron_note = '<p>For Tagalys to keep products and categories in sync, some background processes have to be setup. These have been defined such that they can be triggered via your crontab as per these recommended frequencies. If you have any questions, please contact us.</p>';
        $cron_note .= "<ul>
            <li>*/5 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php sync 500 50</li>
            <li>*/5 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php update-product-positions</li>
            <li>0 2 * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php run-maintenance</li>
            <li>0 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php update-caches</li>
            <li>0 2 * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php trigger-full-sync</li>
        </ul>";
        $cron_fieldset->addField('cront_note', 'note', array(
            'label' => $this->__('For your Tech team'),
            'text' => '<div class="tagalys-note">'. $cron_note.'</div>'
        ));
        
        $fieldset = $form->addFieldset('tagalys_sync_fieldset', array('legend' => $this->__('General Sync Settings')));

        $fieldset->addField('stores_for_tagalys', 'multiselect', array(
            'name'      => 'stores_for_tagalys',
            'style' => "width:100%; height: 125px; display:none;",
            'onclick' => "return false;",
            'onchange' => "return false;",
            'value'  => $this->_helper->getStoresForTagalys(),
            'values' => $this->_helper->getAllWebsiteStores(),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));
        $store_tree_data = htmlspecialchars($this->_helper->getStoreTreeData(), ENT_QUOTES, 'UTF-8');
        $fieldset->addField('stores_jtree_wrap', 'note', array(
            'label' => $this->__('Choose stores for which you want to enable Tagalys features'),
            'text' => "<input id='stores-jtree-q' /><div id='stores-jtree' data-tree=\"{$store_tree_data}\" ></div>"
        ));

        $fieldset->addField('periodic_full_sync', 'select', array(
            'name' => 'periodic_full_sync',
            'label' => 'Daily full sync',
            'title' => 'Daily full sync',
            'options' => array(
                '1' => $this->__('Yes'),
                '0' => $this->__('No')
            ),
            'style' => 'width:100%',
            'after_element_html' => '<small>Use this option if you use tools like Magmi / Unicommerce that work on the Magento database directly. To change the period, please contact us.</small>',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("periodic_full_sync")
        ));

        $image_fieldset = $form->addFieldset('tagalys_sync_images_fieldset', array('legend' => $this->__('Image Settings')));

        $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
        $package_name = Mage::getStoreConfig('tagalys/package/name');
        if ($setup_status != 'completed' && $package_name == 'Search') {
            $image_fieldset->addField('search_box_selector', 'text', array(
                'name'      => 'search_box_selector',
                'label'     => $this->__('Search box selector'),
                'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("search_box_selector"),
                'required'  => true,
                'style'   => "width:100%",
                'after_element_html' => '<small>Please consult with your tech team or <a href="mailto:cs@tagalys.com">contact us</a>. <br>This can be any jQuery selector.<br>Eg: #search / .search-field / [type="search"]</small>',
                'tabindex' => 1
            ));

            $image_fieldset->addField('suggestions_align_to_parent_selector', 'text', array(
                'name'      => 'suggestions_align_to_parent_selector',
                'label'     => $this->__('Align suggestions to search box parent'),
                'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("suggestions_align_to_parent_selector"),
                'required'  => false,
                'style'   => "width:100%",
                'after_element_html' => '<small>If you want to align the search suggestions popup under a parent of the search box instead of the search box itself, specify the selector here.<br>This can be any jQuery selector.<br>Eg: #search-and-icon-container</small>',
                'tabindex' => 1
            ));
        }

        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
        $image_attribute_options = array();
        $image_hover_attribute_options = array('' => 'None');
        foreach ($attributes as $attribute){
          if ($attribute->getFrontendInput() == 'media_image') {
            $image_attribute_options[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
            $image_hover_attribute_options[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
          }
        }
        $image_fieldset->addField('product_image_attribute', 'select', array(
            'name' => 'product_image_attribute',
            'label' => 'Product Image',
            'title' => 'Product Image',
            'options' => $image_attribute_options,
            'required' => true,
            'style' => 'width:100%',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("product_image_attribute"),
            'after_element_html' => '<small>Image attribute to use when displaying products in Tagalys listing pages and recommendations.</small>',
        ));
        $image_fieldset->addField('product_image_hover_attribute', 'select', array(
            'name' => 'product_image_hover_attribute',
            'label' => 'Product Hover Image',
            'title' => 'Product Hover Image',
            'options' => $image_hover_attribute_options,
            'required' => false,
            'style' => 'width:100%',
            'value' => Mage::getModel('tagalys_core/config')->getTagalysConfig("product_image_hover_attribute"),
            'after_element_html' => '<small>Image attribute to use on hover when displaying products in Tagalys listing pages and recommendations.</small>',
        ));

        $image_fieldset->addField('max_product_thumbnail_width', 'text', array(
            'name'      => 'max_product_thumbnail_width',
            'label'     => $this->__('Maximum product thumbnail width'),
            'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("max_product_thumbnail_width"),
            'required'  => true,
            'style'   => "width:100%",
            'after_element_html' => '<small>The maximum width of product thumbnail images in Tagalys pages while maintaining apsect ratios. Smaller images will not be scaled up. <strong>Requires a full resync to take effect.</strong></small>',
            'tabindex' => 1
        ));
        $image_fieldset->addField('max_product_thumbnail_height', 'text', array(
            'name'      => 'max_product_thumbnail_height',
            'label'     => $this->__('Maximum product thumbnail height'),
            'value'  => Mage::getModel('tagalys_core/config')->getTagalysConfig("max_product_thumbnail_height"),
            'required'  => true,
            'style'   => "width:100%",
            'after_element_html' => '<small>The maximum height of product thumbnail images in Tagalys pages while maintaining apsect ratios. Smaller images will not be scaled up. <strong>Requires a full resync to take effect.</strong></small>',
            'tabindex' => 1
        ));

        $save_fieldset = $form->addFieldset('tagalys_sync_save_fieldset', array('legend' => $this->__('Save')));
        $save_fieldset->addField('submit', 'submit', array(
            'name' => 'tagalys_submit_action',
            'value' => 'Save & Continue to Sync',
            'class'=> "tagalys-btn",
            'style'   => "width:100%",
            'onclick' => 'if(this.classList.contains(\'clicked\')) { return false; } else {  this.className += \' clicked\'; var that = this; setTimeout(function(){ that.value=\'Please wait…\'; that.disabled=true; }, 50); return true; }',
            'after_element_html' => '<small><em></em></small>',
            'tabindex' => 1
        ));

        $this->setForm($form);
        return parent::_prepareForm();
    }


    public function getTabLabel() {
        return $this->__('Sync Settings');
    }

        /**
     * Tab title getter
     *
     * @return string
     */
        public function getTabTitle() {
            return $this->__('Sync Settings');
        }

    /**
     * Check if tab can be shown
     *
     * @return bool
     */
    public function canShowTab() {
        return true;
    }

    /**
     * Check if tab hidden
     *
     * @return bool
     */
    public function isHidden() {
        return false;
    }
}