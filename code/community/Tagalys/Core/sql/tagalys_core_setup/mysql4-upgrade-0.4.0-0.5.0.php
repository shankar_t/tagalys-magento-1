<?php
$installer = $this;

$connection = $installer->getConnection();

$installer->startSetup();

$tableName = $installer->getTable('tagalys_core_queue');

if (!$connection->tableColumnExists($tableName, 'store_id')) {
    $connection->addColumn($tableName, 'store_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => null,
        'comment' => 'Store ID'
    ));
}

$connection->addIndex(
    $tableName,
    $installer->getIdxName(
        $tableName,
        array('product_id', 'store_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('product_id', 'store_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->endSetup();