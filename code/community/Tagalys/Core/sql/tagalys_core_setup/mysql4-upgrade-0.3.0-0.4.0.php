<?php
$installer = $this;

$installer->startSetup();

if (!$installer->getConnection()->isTableExists($installer->getTable('tagalys_core_categories'))) {
    $tagalys_core_categories_table = $installer->getConnection()->newTable($installer->getTable('tagalys_core_categories'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
        ), 'ID')
        ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 50, array(
            'nullable' => false
        ), 'Category ID')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 50, array(
            'nullable' => false
        ), 'Store ID')
        ->addColumn('positions_synced_at', Varien_Db_Ddl_Table::TYPE_DATETIME, array(), 'Positions last synced at')
        ->addColumn('positions_sync_required', Varien_Db_Ddl_Table::TYPE_BOOLEAN, array('nullable' => false, 'default' => '0'), 'Positions sync required?')
        ->addColumn('marked_for_deletion', Varien_Db_Ddl_Table::TYPE_BOOLEAN, array('nullable' => false, 'default' => '0'), 'Marked for deletion?')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false, 'default' => ''), 'Status') // pending_sync / powered_by_tagalys / failed
        ->setComment('Tagalys Categories Table');
    $tagalys_core_categories_table->addIndex(
        $installer->getIdxName('tagalys_core/categories', array('store_id', 'category_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('store_id', 'category_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
    $installer->getConnection()->createTable($tagalys_core_categories_table);
}

$installer->endSetup();