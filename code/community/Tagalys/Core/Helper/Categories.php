<?php
class Tagalys_Core_Helper_Categories extends Mage_Core_Helper_Abstract
{
    public function isPositionedByTagalys($storeId, $categoryId) {
        try {
            if ($this->isTagalysCreated($categoryId)) {
                return true;
            }
            $storeMapping = Mage::getModel('tagalys_core/config')->getTagalysConfig("category_pages_store_mapping", true);
            $storeIdToQuery = (array_key_exists($storeId . '', $storeMapping) ? $storeMapping[$storeId.''] : $storeId);
            $firstItem = Mage::getModel('tagalys_core/categories')->getCollection()
                ->addFieldToFilter('store_id', $storeIdToQuery)
                ->addFieldToFilter('category_id', $categoryId)
                ->addFieldToFilter('status', 'powered_by_tagalys')
                ->addFieldToFilter('marked_for_deletion', 0)
                ->getFirstItem();
            if ($id = $firstItem->getId()) {
                return true;
            }
        } catch (Exception $e) {

        }
        return false;
    }
    public function transitionFromCategoriesConfig() {
        $categoryIds = Mage::getModel('tagalys_core/config')->getTagalysConfig("category_ids", true);
        $storesForTagalys = Mage::helper('tagalys_core')->getStoresForTagalys();
        foreach($storesForTagalys as $storeId){
            $currentStore = Mage::app()->getStore();
            Mage::app()->setCurrentStore(Mage::app()->getStore($storeId));
            foreach($categoryIds as $i => $categoryId) {
                $category = Mage::getModel('catalog/category')->load($categoryId);
                $categoryActive = $category->getIsActive();
                if ($categoryActive && ($category->getDisplayMode() != Mage_Catalog_Model_Category::DM_PAGE)) {
                    Mage::getModel("tagalys_core/categories")->createOrUpdateWithData($storeId, $categoryId, array('positions_sync_required' => 0, 'marked_for_deletion' => 0, 'status' => 'pending_sync'), array('marked_for_deletion' => 0));
                }
            }
            Mage::app()->setCurrentStore($currentStore);
        }
    }
    public function maintenanceSync() {
        // once a day

        $listingPagesEnabled = (Mage::getModel('tagalys_core/config')->getTagalysConfig("module:listingpages:enabled") == '1');
        if ($listingPagesEnabled) {
            // 1. try and sync all failed categories - mark positions_sync_required as 1 for all failed categories - this will then try and sync the categories again
            $failedCategories = Mage::getModel('tagalys_core/categories')
                ->getCollection()
                ->addFieldToFilter('status', 'failed')
                ->addFieldToFilter('marked_for_deletion', 0);
            foreach ($failedCategories as $i => $failedCategory) {
                $failedCategory->addData(array('status' => 'pending_sync'))->save();
            }

            // 2. if preference is to power all categories, loop through all categories and add missing items to the tagalys_core_categories table
            // TODO

            // 3. send all category ids to be powered by tagalys - tagalys will delete other ids
            $storesForTagalys = Mage::helper('tagalys_core')->getStoresForTagalys();
            $categoriesForTagalys = array();
            foreach($storesForTagalys as $key=>$storeId){
                $categoriesForTagalys[$storeId] = array();
                $storeCategories = Mage::getModel('tagalys_core/categories')
                    ->getCollection()
                    ->addFieldToFilter('store_id', $storeId);
                foreach ($storeCategories as $i => $storeCategory) {
                    array_push($categoriesForTagalys[$storeId], '__categories--'.$storeCategory->getCategoryId());
                }
            }
            Mage::getSingleton('tagalys_core/client')->clientApiCall('/v1/mpages/_platform/verify_enabled_pages', array('enabled_pages' => $categoriesForTagalys));
        }
        return true;
    }
    public function _checkSyncLock($syncStatus) {
        if ($syncStatus['locked_by'] == null) {
            return true;
        } else {
            // some other process has claimed the thread. if a crash occours, check last updated at < 15 minutes ago and try again.
            $lockedAt = new DateTime($syncStatus['updated_at']);
            $now = new DateTime();
            $intervalSeconds = $now->getTimestamp() - $lockedAt->getTimestamp();
            $minSecondsForOverride = 5 * 60;
            if ($intervalSeconds > $minSecondsForOverride) {
                Mage::getSingleton('tagalys_core/client')->log('warn', 'Overriding stale locked process for categories sync', array('pid' => $syncStatus['locked_by'], 'locked_seconds_ago' => $intervalSeconds));
                return true;
            } else {
                Mage::getSingleton('tagalys_core/client')->log('warn', 'Categories sync locked by another process', array('pid' => $syncStatus['locked_by'], 'locked_seconds_ago' => $intervalSeconds));
                return false;
            }
        }
    }
    public function getRequiresPositionsSyncCollection() {
        $categoriesToSync = Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('status', 'powered_by_tagalys')
            ->addFieldToFilter('positions_sync_required', 1)
            ->addFieldToFilter('marked_for_deletion', 0);
        return $categoriesToSync;
    }
    public function getEnabledCount($storeId) {
        return Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('marked_for_deletion', 0)
            ->count();
    }
    public function getPendingSyncCount($storeId) {
        return Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('status', 'pending_sync')
            ->addFieldToFilter('marked_for_deletion', 0)
            ->count();
    }
    public function getRequiringPositionsSyncCount($storeId) {
        return Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('status', 'powered_by_tagalys')
            ->addFieldToFilter('positions_sync_required', 1)
            ->addFieldToFilter('marked_for_deletion', 0)
            ->count();
    }
    public function markPositionsSyncRequired($storeId, $categoryId) {
        $firstItem = Mage::getModel('tagalys_core/categories')->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('category_id', $categoryId)
            ->getFirstItem();
        if ($id = $firstItem->getId()) {
            $firstItem->addData(array('positions_sync_required' => 1))->save();
        }
        return true;
    }
    public function markPositionsSyncRequiredForCategories($storeId, $categoryIds) {
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $table = Mage::getSingleton('core/resource')->getTableName('tagalys_core_categories');
        $whereData = array(
            'status = ?' => 'powered_by_tagalys'
        );
        if ($storeId !== 'all') {
            $whereData['store_id = ?'] = $storeId;
        }
        if ($categoryIds !== 'all') {
            $whereData['category_id IN (?)'] = $categoryIds;
        }
        $updateData = array(
            'positions_sync_required' => 1
        );
        $conn->update($table, $updateData, $whereData);
        return true;
    }
    public function markFailedCategoriesForRetrying() {
      $failedCategories = Mage::getModel('tagalys_core/categories')
          ->getCollection()
          ->addFieldToFilter('status', 'failed')
          ->addFieldToFilter('marked_for_deletion', 0);
      foreach ($failedCategories as $i => $failedCategory) {
          $failedCategory->addData(array('status' => 'pending_sync'))->save();
      }
      return true;
    }
    public function updatePositionsIfRequired($maxProductsPerCronRun = 50, $perPage = 5, $force = false) {
        $listingPagesEnabled = (Mage::getModel('tagalys_core/config')->getTagalysConfig("module:listingpages:enabled") == '1');
        // $indexerProcess = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
        // $indexerDone = ($indexerProcess->getStatus() == Mage_Index_Model_Process::STATUS_PENDING);
        $indexerDone = true; // update positions anyway. when reindex is done, it will get the right value. on the dashboard, the user should see an appropriate message.
        if (($listingPagesEnabled || $force) && $indexerDone) {
            $pid = Mage::helper('core')->getRandomString(24);
            Mage::getSingleton('tagalys_core/client')->log('local', '1. Started updatePositionsIfRequired', array('pid' => $pid));
            $categoriesSyncStatus = Mage::getModel('tagalys_core/config')->getTagalysConfig("categories_sync_status", true);
            if ($this->_checkSyncLock($categoriesSyncStatus)) {
                $utcNow = new DateTime("now", new DateTimeZone('UTC'));
                $timeNow = $utcNow->format(DateTime::ATOM);
                $syncStatus = array(
                    'updated_at' => $timeNow,
                    'locked_by' => $pid
                );
                Mage::getModel('tagalys_core/config')->setTagalysConfig('categories_sync_status', $syncStatus, true);
                $collection = $this->getRequiresPositionsSyncCollection();
                $remainingCount = $collection->count();
                Mage::log("updatePositionsIfRequired: remainingCount: {$remainingCount}", null, 'tagalys_processes.log', true);
                $countToSyncInCronRun = min($remainingCount, $maxProductsPerCronRun);
                $numberCompleted = 0;
                $circuitBreaker = 0;
                $jsRenderedStores = Mage::getModel('tagalys_core/config')->getTagalysConfig('js_rendered_stores', true);
                while ($numberCompleted < $countToSyncInCronRun && $circuitBreaker < 26) {
                    $circuitBreaker += 1;
                    $categoriesToSync = $this->getRequiresPositionsSyncCollection()->setPageSize($perPage);
                    $utcNow = new DateTime("now", new DateTimeZone('UTC'));
                    $timeNow = $utcNow->format(DateTime::ATOM);
                    $syncStatus['updated_at'] = $timeNow;
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('categories_sync_status', $syncStatus, true);
                    foreach($categoriesToSync as $categoryToSync) {
                        //TODO sync product positions
                        $storeId = $categoryToSync->getStoreId();
                        $categoryId = $categoryToSync->getCategoryId();
                        $category = Mage::getModel('catalog/category')->load($categoryId);
                        if(in_array($storeId, $jsRenderedStores) || ($category->getIsActive() == 0)){
                            $categoryToSync->addData(array('positions_sync_required' => 0, 'positions_synced_at' => date("Y-m-d H:i:s")))->save();
                            continue;
                        }
                        $newPositions = Mage::getSingleton('tagalys_core/client')->storeApiCall($storeId.'', '/v1/mpages/_platform/__categories-'.$categoryId.'/positions', array());
                        if ($newPositions != false) {
                            $this->performCategoryPositionUpdate($storeId, $categoryId, $newPositions['positions']);
                            $categoryToSync->addData(array('positions_sync_required' => 0, 'positions_synced_at' => date("Y-m-d H:i:s")))->save();
                        } else {
                            // api call failed
                        }
                    }
                    $numberCompleted += $categoriesToSync->count();
                    Mage::log("updatePositionsIfRequired: completed {$numberCompleted}", null, 'tagalys_processes.log', true);
                }
                $syncStatus['locked_by'] = null;
                Mage::getModel('tagalys_core/config')->setTagalysConfig('categories_sync_status', $syncStatus, true);
            }
        }
    }

    public function performCategoryPositionUpdate($storeId, $categoryId, $positions) {
        $jsRenderedStores = Mage::getModel('tagalys_core/config')->getTagalysConfig('js_rendered_stores', true);
        if(in_array($storeId, $jsRenderedStores)) {
            return false;
        }
        $sortOrder = Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:position_sort_direction');
        if($sortOrder == 'asc') {
            $this->_updatePositions($storeId, $categoryId, $positions);
        } else {
            $this->_updatePositionsReverse($storeId, $categoryId, $positions);
        }
        return true;
    }

    public function _updatePositions($storeId, $categoryId, $positions) {
        // https://magento.stackexchange.com/questions/52053/set-category-product-position-of-products
        // https://stackoverflow.com/questions/9990716/update-query-in-zend-framework
        // https://stackoverflow.com/questions/15137959/how-to-get-magento-table-name
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $table = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $indexTable = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));

        $pushDown = false;
        $whereData = array(
            'category_id = ?' => (int)$categoryId,
            'position <= ?' => count($positions) + 1
        );
        $updateData = array(
            'position' => (count($positions) + 2)
        );
        if($this->isProductPushDownAllowed($categoryId)){
            $pushDown = true;
        } else {
            $positionOffset = count($positions) + 1;
            $sql = "SELECT product_id FROM $indexTable WHERE category_id = $categoryId AND position <= $positionOffset AND store_id = $storeId AND visibility IN (2, 4); ";
            $result = $conn->fetchAll($sql);
            $productsInStore = array();
            foreach ($result as $row) {
                $productsInStore[] = (int) $row['product_id'];
            }
            if(count($productsInStore) > 0){
                $pushDown = true;
                $productsInStore = implode(', ', $productsInStore);
                $whereData["product_id IN (?)"] = $productsInStore;
            }
        }
        if ($pushDown) {
            $conn->update($table, $updateData, $whereData);
            $conn->update($indexTable, $updateData, $whereData);
        }

        foreach($positions as $productId => $productPosition) {
            $whereData = array(
                'category_id = ?' => (int)$categoryId,
                'product_id = ?' => (int)$productId
            );
            $updateData = array(
                'position' => ((int)$productPosition + 1)
            );
            $conn->update($table, $updateData, $whereData);
            $conn->update($indexTable, $updateData, $whereData);
        }
        $event_data_array = array('category_id' => (int)$categoryId);
        $varien_object = new Varien_Object($event_data_array);
        Mage::dispatchEvent( 'tagalys_category_positions_updated', array('varien_obj'=>$varien_object));
        return true;
    }

    public function _updatePositionsReverse($storeId, $categoryId, $positions) {
        // https://magento.stackexchange.com/questions/52053/set-category-product-position-of-products
        // https://stackoverflow.com/questions/9990716/update-query-in-zend-framework
        // https://stackoverflow.com/questions/15137959/how-to-get-magento-table-name
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $table = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $indexTable = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));

        $pushDown = false;
        $whereData = array(
            'category_id = ?' => (int)$categoryId,
            'position >= ?' => 100
        );
        $updateData = array(
            'position' => 99
        );
        if($this->isProductPushDownAllowed($categoryId)){
            $pushDown = true;
        } else {
            $sql = "SELECT product_id FROM $indexTable WHERE category_id = $categoryId AND position >= 100 AND store_id = $storeId AND visibility IN (2, 4); ";
            $result = $conn->fetchAll($sql);
            foreach ($result as $row) {
                $productsInStore[] = (int) $row['product_id'];
            }
            if(count($productsInStore) > 0){
                $pushDown = true;
                $productsInStore = implode(', ', $productsInStore);
                $whereData["product_id IN (?)"] = $productsInStore;
            }
        }
        if ($pushDown) {
            $conn->update($table, $updateData, $whereData);
            $conn->update($indexTable, $updateData, $whereData);
        }

        $totalPositions = count($positions);

        foreach($positions as $productId => $productPosition) {
            $whereData = array(
                'category_id = ?' => (int)$categoryId,
                'product_id = ?' => (int)$productId
            );
            $updateData = array(
                'position' => 101 + $totalPositions - (int)$productPosition
            );
            $conn->update($table, $updateData, $whereData);
            $conn->update($indexTable, $updateData, $whereData);
        }
        $event_data_array = array('category_id' => (int)$categoryId);
        $varien_object = new Varien_Object($event_data_array);
        Mage::dispatchEvent( 'tagalys_category_positions_updated', array('varien_obj'=>$varien_object));
        return true;
    }

    public function getRemainingForSync() {
        return Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('status', 'pending_sync')
            ->addFieldToFilter('marked_for_deletion', 0)->count();
    }
    public function getRemainingForDelete() {
        return Mage::getModel('tagalys_core/categories')
            ->getCollection()
            ->addFieldToFilter('marked_for_deletion', 1)->count();
    }
    public function syncAll($force = false) {
        $remainingForSync = $this->getRemainingForSync();
        $remainingForDelete = $this->getRemainingForDelete();
        Mage::log('syncAll: '.json_encode(compact('remainingForSync', 'remainingForDelete')), null, 'tagalys_processes.log', true);
        while($remainingForSync > 0 || $remainingForDelete > 0) {
            $this->sync(50, $force);
            $remainingForSync = $this->getRemainingForSync();
            $remainingForDelete = $this->getRemainingForDelete();
            Mage::log('syncAll: '.json_encode(compact('remainingForSync', 'remainingForDelete')), null, 'tagalys_processes.log', true);
        }
    }
    public function sync($max, $force = false) {
        $listingPagesEnabled = (Mage::getModel('tagalys_core/config')->getTagalysConfig("module:listingpages:enabled") == '1');
        $debug = Mage::getModel('tagalys_core/config')->getTagalysConfig("enable_category_sync_debug_logs") == '1';
        if ($listingPagesEnabled || $force) {
            $detailsToSync = array();

            $logData = [
                "categories_to_sync" => [],
                "categories_to_delete" => []
            ];
            // save
            $categoriesToSync = Mage::getModel('tagalys_core/categories')
                ->getCollection()
                ->addFieldToFilter('status', 'pending_sync')
                ->addFieldToFilter('marked_for_deletion', 0)
                ->setPageSize($max);
            foreach ($categoriesToSync as $i => $categoryToSync) {
                $storeId = $categoryToSync->getStoreId();
                if($debug) {
                    array_push($logData["categories_to_sync"], ['store_id' => $storeId, "category_id" => $categoryToSync->getCategoryId() ]);
                }
                array_push($detailsToSync, array('perform' => 'save', 'store_id' => $storeId, 'payload' => $this->getStoreCategoryDetails($storeId, $categoryToSync->getCategoryId())));
            }
            // delete
            $categoriesToDelete = Mage::getModel('tagalys_core/categories')
                ->getCollection()
                ->addFieldToFilter('marked_for_deletion', 1)
                ->setPageSize($max);
            foreach ($categoriesToDelete as $i => $categoryToDelete) {
                $storeId = $categoryToDelete->getStoreId();
                $categoryId = $categoryToDelete->getCategoryId();
                if($debug) {
                    array_push($logData["categories_to_delete"], ['store_id' => $storeId, "category_id" => $categoryId]);
                }
                array_push($detailsToSync, array('perform' => 'delete', 'store_id' => $storeId, 'payload' => array('id' => "__categories-{$categoryId}")));
            }

            if (count($detailsToSync) > 0) {
                // sync
                if($debug) {
                    Mage::log(json_encode(['tagalys_request' => $logData]), null, 'tagalys_categories.log', true);
                }
                $tagalysResponse = Mage::getSingleton('tagalys_core/client')->clientApiCall('/v1/mpages/_sync_platform_pages', array('actions' => $detailsToSync));

                $responseJson = json_encode($tagalysResponse);
                if($debug) {
                    Mage::log(json_encode(["raw_tagalys_response" => $responseJson]), null, 'tagalys_categories.log', true);
                }

                if ($tagalysResponse != false) {
                    foreach($tagalysResponse['save_actions'] as $i => $saveActionResponse) {
                        $firstItem = Mage::getModel('tagalys_core/categories')->getCollection()
                        ->addFieldToFilter('store_id', $saveActionResponse['store_id'])
                        ->addFieldToFilter('category_id', explode('-', $saveActionResponse['id'])[1])
                        ->getFirstItem();

                        $storeId = $saveActionResponse['store_id'];
                        $categoryId = explode('-', $saveActionResponse['id'])[1];
                        $firstItemId = $firstItem->getId();
                        if($debug) {
                            Mage::log(json_encode(["message" => "Marking category as powered_by_tagalys", "storeId" => $storeId, "categoryId" => $categoryId, "_id" => $firstItemId]), null, 'tagalys_categories.log', true);
                        }

                        if ($id = $firstItem->getId()) {
                            if ($saveActionResponse['saved']) {
                                $firstItem->addData(array('status' => 'powered_by_tagalys', 'positions_sync_required' => 1))->save();
                            } else {
                                $firstItem->addData(array('status' => 'failed'))->save();
                            }
                        }
                        if($debug) {
                            Mage::log(json_encode(["message" => "Marked category as powered_by_tagalys", "storeId" => $storeId, "categoryId" => $categoryId, "_id" => $firstItemId]), null, 'tagalys_categories.log', true);
                        }
                    }
                    foreach ($categoriesToDelete as $i => $categoryToDelete) {
                        $categoryItemId = $categoryToDelete->getId();
                        $categoryId = $categoryToDelete->getCategoryId();
                        $storeId = $categoryToDelete->getStoreId();
                        if($debug) {
                            Mage::log(json_encode(["message" => "Removing category item", "storeId" => $storeId, "categoryId" => $categoryId, "_id" => $categoryItemId]), null, 'tagalys_categories.log', true);
                        }
                        $categoryToDelete->delete();
                        if($debug) {
                            Mage::log(json_encode(["message" => "Removed category item", "storeId" => $storeId, "categoryId" => $categoryId, "_id" => $categoryItemId]), null, 'tagalys_categories.log', true);
                        }
                    }
                }
            }
        }
    }

    public function getStoreCategoryDetails($storeId, $categoryId) {
        $currentStore = Mage::app()->getStore();
        Mage::app()->setCurrentStore(Mage::app()->getStore($storeId));
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $categoryDetails = array(
            "id" => "__categories-$categoryId",
            "slug" => $category->getUrl(),
            "enabled" => ($category->getIsActive() == 1),
            "name" => implode(' / ', array_slice(explode(' |>| ', Mage::getModel('tagalys_core/config')->getCategoryName($category)), 1)),
            "filters" => array(
                array(
                    "field" => "__categories",
                    "value" => $categoryId
                ),
                array(
                    "field" => "visibility",
                    "tag_jsons" => array("{\"id\":\"2\",\"name\":\"Catalog\"}", "{\"id\":\"4\",\"name\":\"Catalog, Search\"}")
                )
            )
        );
        Mage::app()->setCurrentStore($currentStore);
        return $categoryDetails;
    }

    public function uiPoweredByTagalys($storeId, $categoryId) {
        try {
            $firstItem = Mage::getModel('tagalys_core/categories')->getCollection()
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('category_id', $categoryId)
                ->addFieldToFilter('status', 'powered_by_tagalys')
                ->addFieldToFilter('marked_for_deletion', 0)
                ->getFirstItem();

            if ($id = $firstItem->getId()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function isMultiStoreWarningRequired() {
        $allStores = Mage::helper('tagalys_core')->getAllWebsiteStores();
        $showMultiStoreConfig = false;
        $checkAllCategories = false;
        if(count($allStores)>1){
            $rootCategories = array();
            foreach($allStores as $store){
                $rootCategoryId = Mage::app()->getStore($store['value'])->getRootCategoryId();
                if(in_array($rootCategoryId, $rootCategories)){
                    $checkAllCategories = true;
                    break;
                } else {
                    array_push($rootCategories,$rootCategoryId);
                }
            }
        }

        if($checkAllCategories){
            $allCategories = array();
            foreach($allStores as $store){
                $rootCategoryId = Mage::app()->getStore($store['value'])->getRootCategoryId();
                $categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->setStoreId($store['value'])
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
                    ->addAttributeToSelect('id');
                foreach($categories as $cat){
                    if(in_array($cat->getId(),$allCategories)){
                        $showMultiStoreConfig = true;
                        break;
                    } else {
                        array_push($allCategories,$cat->getId());
                    }
                }
            }
        }

        return $showMultiStoreConfig;
    }

    public function isProductPushDownAllowed($categoryId){
        $activeInStores = 0;
        $allStores = Mage::helper('tagalys_core')->getAllWebsiteStores();
        if(count($allStores)==1){
            // Single Store
            return true;
        }
        // Multiple Stores
        foreach($allStores as $store){
            $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->setStoreId($store['value'])
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('entity_id', $categoryId)
                ->addAttributeToSelect('id');

            if($categories->count()>0){
                $activeInStores++;
                if($activeInStores>1){
                    return (Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:same_or_similar_products_across_all_stores") == '1');
                }
            }
        }
        return true;
    }

    public function pushDownProductsIfRequired($productIds, $productCategories = null, $indexerToRun = 'product') {
        // This whole thing is not tested yet!!
        // called from observers when new products are added to categories - in position ascending order, they should be positioned at the bottom of the page.
        $listingpagesEnabled = Mage::getModel('tagalys_core/config')->getTagalysConfig('module:listingpages:enabled');
        $sortDirection = Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:position_sort_direction');
        $categoryProductIndexer =Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
        if($listingpagesEnabled == '1' && $sortDirection == 'asc' && !$categoryProductIndexer->isScheduled()) {
            $tagalysCategories = $this->getTagalysCategories($productCategories);
            foreach($tagalysCategories as $categoryId){
                $storeIds = $this->getTagalysCategoryStores($categoryId);
                foreach($storeIds as $storeId){
                    $productIds = [];
                    $productCollection = Mage::getModel('catalog/product')->getCollection()
                                ->addAttributeToFilter('status', 1)
                                ->addStoreFilter($storeId)
                                ->addAttributeToFilter('category_id', $categoryId)
                                ->addAttributeToFilter('entity_id', ['in' => $productIds])
                                ->addAttributeToFilter('visibility', ["in" => [2,4]])
                                ->addAttributeToSelect('entity_id')
                                ->load();
                    foreach($productCollection as $product){
                        $productIds[] = $product->getId();
                    }
                    $this->pushDownProducts($productIds, $categoryId, $indexerToRun);
                }
            }
            if($this->isMultiStoreWarningRequired()){
                // FIXME
                $storeIds = Mage::getModel('tagalys_core/config')->getTagalysConfig();
                // [1, 2, 3] => {1 => 2} (1,2 have same products) => [1, 3]
                $tableName = $this->resourceConnection->getTableName('catalog_category_product_index_store_1');
                // A given product is present in any of the Tagalys powered category in any of the Tagalys powered store with visibility 2 or 4.
                $sql = "SELECT product_id FROM $tableName WHERE store_id IN ({implode(',',$storeIds)}) AND category_id IN ({implode(',',$tagalysCategories)}) AND product_id IN ({implode(',',$productIds)}) AND visibility IN (2,4) AND position=0;";
                $rows = $this->runSqlSelect($sql);
                $filteredProductIds = [];
                foreach($rows as $row) {
                    $filteredProductIds[] = $row['product_id'];
                }
                $this->pushDownProducts($filteredProductIds, $tagalysCategories, $indexerToRun);
            } else {
                $this->pushDownProducts($productIds, $tagalysCategories, $indexerToRun);
            }
        }
    }

    private function pushDownProducts($productIds, $categoryIds, $reindexProducts) {
        if (count($categoryIds) > 0) {
            $whereData = array(
                'category_id IN (?)' => $categoryIds,
                'product_id IN (?)' => $productIds,
                'position = ?' => 0
            );
            $updateData = array(
                'position' => 9999
            );
            $this->runSqlForCategoryPositions($updateData, $whereData);
            $categoryProductIndexer = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
            if ($categoryProductIndexer->isScheduled() === false) {
                // reindex if index mode is update on save. not required for schedule because we don't want these showing up so it has the same effect. when tagalys syncs and updates positions, reindex will be triggered.
                if($reindexProducts) {
                    $reindexAndClearCacheImmediately = Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:reindex_and_clear_cache_immediately');
                    if ($reindexAndClearCacheImmediately == '1') {
                        $this->reindexProducts($productIds);
                    }
                    // cache may already be getting updated. even otherwise, we don't need to update here as we don't want products to show up anyway. caches are cleared when positions are updated via Tagalys.
                }
            }
        }
    }

    private function getTagalysCategoryStores($categoryIds) {
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $tableName = Mage::getSingleton('core/resource')->getTableName('tagalys_category');
        $select = $conn->select()->from($tableName)->where("marked_for_deletion = 0 and status != 'failed'");
        if($categoryIds){
            $select->where('category_id IN (?)', $categoryIds);
        }
        $result = $conn->fetchAll($select);
        $stores = [];
        foreach($result as $row){
            $stores[] = $row['store_id'];
        }
        return array_unique($stores);
    }

    public function createTagalysParentCategory($storeId, $categoryDetails) {
        $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
        $categoryDetails['is_active'] = false;
        $categoryId = $this->_createCategory($rootCategoryId, $categoryDetails);
        if($categoryId){
            Mage::getModel('tagalys_core/config')->setTagalysConfig("tagalys_parent_category_store_$storeId", $categoryId);
            $this->reindexFlatCategories();
            return $categoryId;
        }
        return false;
    }

    public function createCategory($storeId, $categoryDetails) {
        $parentCategoryId = Mage::getModel('tagalys_core/config')->getTagalysConfig("tagalys_parent_category_store_$storeId");
        $parentCategoryId = Mage::getModel('catalog/category')->load($parentCategoryId)->getId();
        if ($parentCategoryId == null) {
            throw new \Exception("Tagalys parent category not created. Please enable Smart Categories in Tagalys Configuration.");
        }
        $categoryDetails['is_active'] = true;
        $categoryDetails['include_in_menu'] = false;
        $categoryDetails['default_sort_by'] = 'position';
        $categoryDetails['display_mode'] = 'PRODUCTS';
        $categoryId = $this->_createCategory($parentCategoryId, $categoryDetails);
        $this->reindexFlatCategories();
        return $categoryId;
    }

    public function updateCategoryDetails($categoryId, $categoryDetails) {
        Mage::log("updateCategoryDetails: parentId: $categoryId, categoryDetails: ".json_encode($categoryDetails), null, 'tagalys_categories.log', true);
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $category = Mage::getModel('catalog/category', array('disable_flat' => true))->load($categoryId);
        if($category->getId() == null){
            throw new \Exception("Platform category not found");
        }
        $this->validateUrlKey($category->getParentId(), $categoryDetails);
        $categoryDetails['default_sort_by'] = 'position';
        $category->setStoreId(0)->addData($categoryDetails);
        $category->save();
        $this->categoryUpdateAfter($category);
        $this->reindexFlatCategories();
        return true;
    }

    public function categoryUpdateAfter($category){
        // TODO: Remove on future releases
        // if($this->isLegacyMpageCategory($category)){
            // $urlRewrite = $this->urlRewriteCollection->addFieldToFilter('target_path', "catalog/category/view/id/{$category->getId()}")->getFirstItem();
            // $urlRewrite->setRequestPath("m/{$category->getUrlKey()}")->save();
        // }
    }

    public function deleteTagalysCategory($categoryId) {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $category = Mage::getModel('catalog/category', array('disable_flat' => true))->load($categoryId);
        if($category->getId() === null){
            return true;
        }
        $allowDelete = $this->isTagalysCreated($categoryId);
        if($allowDelete){
            Mage::register("isSecureArea", 1);
            $category->delete();
            return true;
        }
        throw new \Exception("This category cannot be deleted because it wasn't created by Tagalys");
    }

    private function _createCategory($parentId, $categoryDetails){
        Mage::log("_createCategory: parentId: $parentId, categoryDetails: ".json_encode($categoryDetails), null, 'tagalys_categories.log', true);
        $this->validateUrlKey($parentId, $categoryDetails);
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $category = Mage::getModel('catalog/category', array('disable_flat' => true));
        $parent = Mage::getModel('catalog/category', array('disable_flat' => true))->load($parentId);
        $category->setData($categoryDetails);
        $category->addData([
            'parent_id' => $parentId,
            'path' => $parent->getPath(),
            'default_sort_by' => 'position',
            'display_mode' => 'PRODUCTS',
            'include_in_menu' => 0,
            'is_anchor' => true
        ]);
        $category->setStoreId(0);
        $category->save();
        return $category->getId();
    }

    public function bulkAssignProductsToCategoryAndRemove($storeId, $categoryId, $productPositions) {
        if($this->isTagalysCreated($categoryId)){
            $productsToRemove = $this->getProductsNotIn($categoryId, $productPositions);
            $this->bulkAssignProductsToCategoryViaDb($storeId, $categoryId, $productPositions);
            $this->_paginateSqlRemove($categoryId, $productsToRemove);
            $this->clearCategoryCache($categoryId);
            return true;
        }
        throw new \Exception("Error: this category wasn't created by Tagalys");
    }

    private function clearCategoryCache($categoryId){
        $event_data_array = array('category_id' => (int)$categoryId);
        $varien_object = new Varien_Object($event_data_array);
        Mage::dispatchEvent( 'tagalys_category_positions_updated', array('varien_obj'=>$varien_object));
    }

    public function reindexFlatCategories(){
        $useFlatCategories = Mage::getStoreConfig('catalog/frontend/flat_catalog_category');
        if ($useFlatCategories == '1') {
            Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_flat')->reindexAll();
        }
    }

    private function getProductsNotIn($categoryId, $productPositions){
        $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $sql = "SELECT product_id FROM $tableName WHERE category_id=$categoryId";
        $select = $this->runSqlSelect($sql);
        $productsInTable = [];
        foreach($select as $row){
            $productsInTable[] = $row['product_id'];
        }
        return array_diff($productsInTable, $productPositions);
    }

    public function bulkAssignProductsToCategoryViaDb($storeId, $categoryId, $productPositions) {
        if(count($productPositions)>0){
            if(Mage::getModel('tagalys_core/config')->isSortedReverse()){
                $productPositions = array_reverse($productPositions);
            }
            $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
            $sql = "REPLACE $tableName (category_id, product_id, position) VALUES ";
            $this->paginateSqlInsert($sql, $categoryId, $productPositions);
            $this->insertIntoCategoryIndex($storeId, $categoryId, $productPositions);
        }
    }

    private function paginateSqlInsert($sql, $categoryId, $productPositions) {
        $rows = [];
        $productCount = count($productPositions);
        for ($index=1; $index <= $productCount; $index++) {
            $productId = $productPositions[$index-1];
            if($productId==2){
                continue;
            }
            $rows[] = "($categoryId, $productId, $index)";
            if( $index % 100 == 0 || $index == $productCount ){
                $values = implode(', ', $rows);
                $query = $sql . $values . ';';
                $rows = [];
                $this->runSql($query);
            }
        }
    }

    private function insertIntoCategoryIndex($storeId, $categoryId, $productPositions){
        $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
        $sql = "REPLACE $tableName (store_id, category_id, product_id, position, visibility, is_parent) VALUES ";
        $rows = [];
        $productCount = count($productPositions);
        for ($index=1; $index <= $productCount; $index++) {
            $productId = $productPositions[$index-1];
            if($productId==2){
                continue;
            }
            $rows[] = "($storeId, $categoryId, $productId, $index, 4, 1)";
            if( $index % 100 == 0 || $index == $productCount ){
                $values = implode(', ', $rows);
                $query = $sql . $values . ';';
                $rows = [];
                $this->runSql($query);
            }
        }
    }

    private function _paginateSqlRemove($categoryId, $products){
        $perPage = 100;
        $offset = 0;
        $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $indexTableName = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
        $productsToDelete = array_slice($products, $offset, $perPage);
        while(count($productsToDelete)>0){
            $productsToDelete = implode(', ', $productsToDelete);
            $sql = "DELETE FROM $tableName WHERE category_id=$categoryId AND product_id IN ($productsToDelete);";
            $this->runSql($sql);
            $sql = "DELETE FROM $indexTableName WHERE category_id=$categoryId AND product_id IN ($productsToDelete);";
            $this->runSql($sql);
            $offset += $perPage;
            $productsToDelete = array_slice($products, $offset, $perPage);
        }
    }

    private function runSql($sql) {
        // Not for SELECT
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $conn->query($sql);
    }
    private function runSqlSelect($sql) {
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        return $conn->fetchAll($sql);
    }
    private function reindexProducts($products){
        foreach($products as $product){
            $event = Mage::getSingleton('index/indexer')->logEvent(
                $product,
                $product->getResource()->getType(),
                Mage_Index_Model_Event::TYPE_SAVE,
                false
            );
            Mage::getSingleton('index/indexer')
                ->getProcessByCode('catalog_category_product') // Adjust the indexer process code as needed
                ->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)
                ->processEvent($event);
        }
    }

    public function isTagalysCreated($category) {
        // called from frontend render
        if(!is_object($category)){
            $category = Mage::getModel('catalog/category')->load($category);
            if(!$category->getId()){
                return false;
            }
        }
        $parentCategories = $this->getTagalysParentCategory();
        if(in_array($category->getId(), $parentCategories)){
            return true;
        }
        if($category->getId()){
            $parentId = $category->getParentId();
            if(in_array($parentId, $parentCategories)){
                return true;
            }
        }
        return false;
    }

    public function getTagalysParentCategory($storeId=null){
        if (isset($storeId)){
            $categoryId = Mage::getModel('tagalys_core/config')->getTagalysConfig("tagalys_parent_category_store_$storeId");
        } else {
            $categoryId = [];
            $websiteStores = Mage::helper('tagalys_core')->getAllWebsiteStores();
            foreach($websiteStores as $sid){
                $sid = $sid['value'];
                $catId = Mage::getModel('tagalys_core/config')->getTagalysConfig("tagalys_parent_category_store_$sid");
                if($catId != null){
                    $categoryId[] = $catId;
                }
            }
        }
        return $categoryId;
    }

    public function getTagalysCreatedCategories(){
        $tagalysCreated = $this->getTagalysParentCategory();
        $tagalysLegacyCategories = Mage::getModel('tagalys_core/config')->getTagalysConfig('legacy_mpage_categories', true);
        $tagalysCreated = array_merge($tagalysCreated, $tagalysLegacyCategories);
        $categories = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('entity_id');
        foreach ($categories as $category) {
            if (in_array($category->getParentId(), $tagalysCreated)) {
                $tagalysCreated[] = $category->getId();
            }
        }
        return $tagalysCreated;
    }

    public function redirectToCategoryUrl($storeId, $categoryId, $path){
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $urlPath = $category->getUrlPath();
        if( substr($category->getUrl(), -5) === '.html' ){
            $urlPath.='.html';
        }
        $this->createUrlRewrite($storeId, $path, $urlPath, 301);
    }

    private function createUrlRewrite($storeId, $requestPath, $targetPath, $redirectType) {
        $urlRewriteModel = Mage::getModel('core/url_rewrite');
        $urlRewriteModel->setStoreId($storeId);
        $urlRewriteModel->setIsSystem(0);
        $urlRewriteModel->setRequestPath($requestPath);
        $urlRewriteModel->setTargetPath($targetPath);
        $urlRewriteModel->setRedirectType($redirectType);
        $urlRewriteModel->setDescription("Created by Tagalys");
        $urlRewriteModel->save();
    }

    public function updateCategoryUrlRewrite($storeId, $categoryId, $path){
        $targetPath = "catalog/category/view/id/$categoryId";
        $urlRewrite = Mage::getModel('core/url_rewrite')->getCollection()->addStoreFilter($storeId)->addFieldToFilter('target_path', $targetPath)->addFieldToFilter('is_autogenerated', 1)->getFirstItem();
        $urlRewrite->setRequestPath($path);
        $urlRewrite->save();
    }

    private function isLegacyMpageCategory($category){
        // check this and remove .html from category on save observer?
        $parentId = $category->getParentId();
        $legacyCategories = Mage::getModel('tagalys_core/config')->getConfig('legacy_mpage_categories', true);
        return in_array($parentId,$legacyCategories);
    }

    private function validateUrlKey($parentId, $categoryDetails, $categoryId = false){
        if(array_key_exists('url_key', $categoryDetails)){
            $categoryExist = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToFilter('url_key', $categoryDetails['url_key'])
                ->addAttributeToFilter('parent_id', $parentId)
                ->addAttributeToSelect('entity_id')
                ->getFirstItem()
                ->getId();
            if($categoryExist){
                if ($categoryId && $categoryId == $categoryExist){
                    return true;
                }
                Mage::log("category create or update failed with error: url_key already exist parentId: $parentId, categoryDetails: ".json_encode($categoryDetails), null, 'tagalys_categories.log', true);
                throw new \Exception("Error: The url key specified already exist.");
            }
        }
    }

    public function transitionMpages($keepOldUrl, $storeIds){
        try{
            foreach($storeIds as $storeId){
                // $output->writeln("Fetching mpages from Tagalys");
                $response = Mage::getSingleton('tagalys_core/client')->clientApiCall('/v1/mpages/get_store_mpages', ['store_id'=>$storeId]);
                if($keepOldUrl === 1){
                    // $output->writeln("Creating Legacy Tagalys Category");
                    $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
                    $legacyRootCategory = $this->_createCategory($rootCategoryId, ['name'=>'Tagalys Legacy Pages', 'url_key'=>'m', 'is_active'=>false]);
                    $legacyCategories = Mage::getModel('tagalys_core/config')->getConfig('legacy_mpage_categories', true);
                    $legacyCategories[] = $legacyRootCategory;
                    Mage::getModel('tagalys_core/config')->setConfig('legacy_mpage_categories', $legacyCategories, true);
                }
                foreach ($response['mpages'] as $mpage) {
                    // $output->writeln("Transferring Mpage: {$mpage['details']['name']}");
                    $categoryId = $this->_createCategory($legacyRootCategory, $mpage['details']);
                    if($keepOldUrl === 1){
                        $this->updateCategoryUrlRewrite($storeId, $categoryId, 'm/'.$mpage['details']['url_key']);
                    } else {
                        $this->redirectToCategoryUrl($storeId, $categoryId, 'm/'.$mpage['details']['url_key']);
                    }
                    $res = Mage::getSingleton('tagalys_core/client')->clientApiCall('/v1/mpages/set_platform_id', ['platform_id'=>$categoryId, 'mpage_id'=>$mpage['id']]);
                    if(!$res){
                        throw new \Exception("Error while sending categoryId $categoryId for mpage {$mpage['es_id']} to Tagalys.");
                    }
                }
            }
        } catch(\Exception $e){
            // $output->writeln('ERROR: '.$e->getMessage());
        }
    }
}
