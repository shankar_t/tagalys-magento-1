<?php

class Tagalys_Core_Helper_Data extends Mage_Core_Helper_Abstract {

    public function isTagalysModuleEnabled($module) {
        $store_id = Mage::app()->getStore()->getId();
        $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
        if ($setup_status == 'completed') {
            $store_setup_completed = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:setup_complete");
            if ($store_setup_completed == '1') {
                $module_enabled = Mage::getModel('tagalys_core/config')->getTagalysConfig("module:$module:enabled");
                if ($module_enabled == '1') {
                    return true;
                }
            }
        }
        return false;
    }

    public function updateTagalysHealth() {
        foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
            $response = Mage::getSingleton('tagalys_core/client')->storeApiCall($store_id.'', '/v1/mpages/_health', array('timeout' => 10));
            if ($response != false && $response['total'] > 0) {
                Mage::getModel('tagalys_core/config')->setTagalysConfig("tagalys:health", '1');
                $this->updateConfigIfRequired($response);
                return true;
            } else {
                Mage::getModel('tagalys_core/config')->setTagalysConfig("tagalys:health", '0');
                return false;
            }
        }
    }

    public function updateConfigIfRequired($response) {
        if (array_key_exists('tagalys_config', $response) && is_array($response['tagalys_config'])) {
            $configs = $response['tagalys_config'];
            if(count($configs) > 0) {
                Mage::getSingleton('tagalys_core/client')->log('info', 'Updated Tagalys configuration', array('configs' => $configs));
                foreach($configs as $_ => $config) {
                    Mage::getModel('tagalys_core/config')->setTagalysConfig($config['path'], $config['value'], $config['json_encode']);
                }
                return true;
            }
        }
        return false;
    }

    public function assignParentCategoriesToAllProducts() {
        // https://inchoo.net/magento/working-with-large-magento-collections/
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', array("neq" => 1))
            ->addAttributeToSelect('entity_id');
        Mage::log("assignParentCategoriesToAllProducts: total: ".$collection->count(), null, 'tagalys_processes.log', true);
        Mage::getSingleton('core/resource_iterator')->walk($collection->getSelect(), array(array($this, 'assignParentCategoriesToProductHandler')));
    }
    public function assignParentCategoriesToProductHandler($args) {
        $this->assignParentCategoriesToProductId($args['row']['entity_id']);
    }

    public function assignParentCategoriesToProductId($productId) {
        Mage::log("assignParentCategoriesToProductId: ".$productId, null, 'tagalys_processes.log', true);
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
        $product = Mage::getModel('catalog/product')->load($productId);
        $productCategoryIds = $product->getCategoryIds();
        $categoriesAssigned = array();
        foreach($productCategoryIds as $categoryId){
            $category = Mage::getModel('catalog/category')->load($categoryId);
            foreach ($category->getParentCategories() as $parent) {
                if ((int)$parent->getLevel() > 1) {
                    if(!in_array($parent->getId(), $productCategoryIds)){
                        if (!in_array($parent->getId(), $categoriesAssigned)) {
                            $this->assignProductToCategoryViaDb($parent->getId(), $product);
                            array_push($categoriesAssigned, $parent->getId());
                        }
                    }
                }
            }
        }
        return true;
    }

    public function shouldSyncAttribute($attribute, $includeVisibilityAttribute = true) {
        $attributeCode = $attribute->getAttributeCode();
        $ignored = ['status', 'tax_class_id'];
        if(!in_array($attributeCode, $ignored)) {
            if($includeVisibilityAttribute) {
                $isVisibilityAttribute = ((bool)$attribute->getIsUserDefined() == false && in_array($attributeCode, array('visibility')));
                if($isVisibilityAttribute) {
                    return true;
                }
            }
            $isForDisplay = ((bool)$attribute->getUsedInProductListing() && (bool)$attribute->getIsUserDefined());
            $isFilterable = $attribute->getIsFilterable();
            $isSearchable = $attribute->getIsSearchable();
            if($isForDisplay || $isFilterable || $isSearchable) {
                return true;
            }
            $whitelistedAttributes = Mage::getModel('tagalys_core/config')->getTagalysConfig("sync:whitelisted_attributes", true);
            if(in_array($attributeCode, $whitelistedAttributes)) {
                // manually whitelisted
                return true;
            }
        }
        return false;
    }

    public function detailsFromCategoryTree($categoriesTree, $storeId) {
        $detailsTree = array();
        foreach($categoriesTree as $categoryId => $subCategoriesTree) {
            $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($categoryId);
            $categoryEnabled = (($category->getIsActive() === true || $category->getIsActive() === '1') ? true : false);
            $categoryIncludedInMenu = (($category->getIncludeInMenu() === true || $category->getIncludeInMenu() === '1') ? true : false);
            $thisCategoryDetails = array("id" => $category->getId() , "label" => $category->getName(), "is_active" => $categoryEnabled, "include_in_menu" => $categoryIncludedInMenu);
            $subCategoriesCount = count($subCategoriesTree);
            if ($subCategoriesCount > 0) {
                $thisCategoryDetails['items'] = $this->detailsFromCategoryTree($subCategoriesTree, $storeId);
            }
            array_push($detailsTree, $thisCategoryDetails);
        }
        return $detailsTree;
    }

    public function mergeIntoCategoriesTree($categoriesTree, $pathIds) {
        $pathIdsCount = count($pathIds);
        if (!array_key_exists($pathIds[0], $categoriesTree)) {
            $categoriesTree[$pathIds[0]] = array();
        }
        if ($pathIdsCount > 1) {
            $categoriesTree[$pathIds[0]] = $this->mergeIntoCategoriesTree($categoriesTree[$pathIds[0]], array_slice($pathIds, 1));
        }
        return $categoriesTree;
    }

    public function getProductCategories($id, $storeId) {
        $product = Mage::getModel('catalog/product')->load($id);
        $categoryIds =  $product->getCategoryIds();
        $activeCategoryPaths = array();
        $categoriesAssigned = array();
        foreach ($categoryIds as $key => $value) {
            $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($value);
            if ($category->getIsActive()) {
                $path = $category->getPath();
                $activeCategoryPaths[] = $path;

                // assign to parent categories
                $exploded = explode('/', $path);
                $relevantCategories = array_slice($exploded, 2); // ignore level 0 and 1
                $idsToAssign = array_diff($relevantCategories, $categoryIds);
                foreach ($idsToAssign as $key => $categoryId) {
                    if (!in_array($categoryId, $categoriesAssigned)) {
                        $this->assignProductToCategoryViaDb($categoryId, $product);
                        array_push($categoriesAssigned, $categoryId);
                    }
                }
                if (count($categoriesAssigned) > 0) {
                    $this->reindexProduct($product);
                }
            }
        }
        $activeCategoriesTree = array();
        foreach($activeCategoryPaths as $activeCategoryPath) {
            $pathIds = explode('/', $activeCategoryPath);
            // skip the first two levels which are 'Root Catalog' and the Store's root
            $pathIds = array_splice($pathIds, 2);
            if (count($pathIds) > 0) {
                $activeCategoriesTree = $this->mergeIntoCategoriesTree($activeCategoriesTree, $pathIds);
            }
        }
        $activeCategoryDetailsTree = $this->detailsFromCategoryTree($activeCategoriesTree, $storeId);
        return $activeCategoryDetailsTree;
    }

    public function assignProductToCategoryViaDb($categoryId, $product){
        $productId = $product->getId();
        Mage::log("assignProductToCategoryViaDb: $productId => $categoryId ", null, 'tagalys_processes.log', true);
        $conn = Mage::getModel('core/resource')->getConnection('core_write');
        $table = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $sortDirection = Mage::getModel('tagalys_core/config')->getTagalysConfig('listing_pages:position_sort_direction');
        $positionToAssign = ($sortDirection == 'desc' ? 1 : 9999);
        $assignData = array('category_id' => (int)$categoryId, 'product_id' => (int)$productId, 'position' => $positionToAssign);
        $conn->insert($table, $assignData);
    }

    public function reindexProduct($product){
        $event = Mage::getSingleton('index/indexer')->logEvent(
            $product,
            $product->getResource()->getType(),
            Mage_Index_Model_Event::TYPE_SAVE,
            false
        );

        Mage::getSingleton('index/indexer')
            ->getProcessByCode('catalog_category_product')
            ->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)
            ->processEvent($event);
    }

    public function getStoresForTagalys() {
        $config_stores = Mage::getModel('tagalys_core/config')->getTagalysConfig("stores");
        
        if ($config_stores) {
            $stores_for_tagalys = json_decode($config_stores, true);
            return $stores_for_tagalys;
        }
        return array();
    }

    public function getTagalysStoresForSelect() {
        $config_stores = Mage::getModel('tagalys_core/config')->getTagalysConfig("stores");
        if ($config_stores) {
            $allStores = $this->getAllWebsiteStores();
            $storesForTagalys = json_decode($config_stores, true);
            $storesForSelect = array();
            foreach($allStores as $store) {
                if(in_array($store['value'], $storesForTagalys)) {
                    $storesForSelect[] = $store;
                }
            }
            return $storesForSelect;
        }
        return array();
    }

    public function getAllWebsiteStores() {
        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $website_stores[] = array("value" => $store->getId(), "label" => $website->getName()." / ".$group->getName(). " / ".$store->getName());
                }
            }
        }
        return $website_stores;
    }

    public function getStoreTreeData(){
      $stores = $this->getAllWebsiteStores();
      $stores_for_tagalys = $this->getStoresForTagalys();
      $tree = array();
      foreach($stores as $store){
          $selected = false;
          foreach($stores_for_tagalys as $selected_store){
            if($store['value']==$selected_store){
                $selected = true;
                $tree[]=array('id'=>$store['value'], 'value'=>$store['value'], 'text'=>$store['label'], 'state'=>array('selected'=>'true'));
            }
          }
          if(!$selected){
            $tree[]=array('id'=>$store['value'], 'value'=>$store['value'], 'text'=>$store['label']);
          }
      }
      return json_encode($tree);
    }

    public function getJsRenderedStoreTreeData() {
        $stores = $this->getAllWebsiteStores();
        $storesForTagalys = $this->getStoresForTagalys();
        $jsRenderedStores = $this->getJsRenderedStores();
        $tree = array();
        foreach($stores as $store){
            if(in_array($store['value'], $storesForTagalys)) {
                $selected = in_array($store['value'], $jsRenderedStores);
                $tree[]=array('id'=>$store['value'], 'value'=>$store['value'], 'text'=>$store['label'], 'state'=>array('selected'=> $selected));
            }
        }
        return json_encode($tree);
    }

    public function getJsRenderedStores(){
        $jsRenderedStores = Mage::getModel('tagalys_core/config')->getTagalysConfig("js_rendered_stores");
        if ($jsRenderedStores) {
            $jsRenderedStores = json_decode($jsRenderedStores, true);
            return $jsRenderedStores;
        }
        return [];
    }

    public function getSyncStatus() {
        $storesSyncRequired = false;
        $waitingForTagalys = false;
        $resyncScheduled = false;
        $syncStatus = array();
        $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
        $setup_complete = ($setup_status == 'completed');
        $syncStatus['setup_complete'] = $setup_complete;
        $syncStatus['stores'] = array();
        foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $key => $store_id) {
            $this_store = array();
            
            $this_store['name'] = Mage::getModel('core/store')->load($store_id)->getName();
            
            $store_setup_complete = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:setup_complete");
            $this_store['setup_complete'] = ($store_setup_complete == '1');

            $store_feed_status = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:feed_status", true);
            if ($store_feed_status != null) {
                $status_for_client = '';
                switch($store_feed_status['status']) {
                    case 'pending':
                        $status_for_client = 'Waiting to write to file';
                        $storesSyncRequired = true;
                        break;
                    case 'processing':
                        $status_for_client = 'Writing to file';
                        $storesSyncRequired = true;
                        break;
                    case 'generated_file':
                        $status_for_client = 'Generated file. Sending to Tagalys.';
                        $storesSyncRequired = true;
                        break;
                    case 'sent_to_tagalys':
                        $status_for_client = 'Waiting for Tagalys';
                        $waitingForTagalys = true;
                        break;
                    case 'finished':
                        $status_for_client = 'Finished';
                        break;
                }
                $store_resync_required = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:resync_required");
                if ($store_resync_required == '1') {
                    $resyncScheduled = true;
                    if ($status_for_client == 'Finished') {
                        $status_for_client = 'Scheduled at 1 AM';
                    }
                }
                if ($status_for_client == 'Writing to file' || $status_for_client == 'Waiting to write to file') {
                    $completed_percentage = round(((int)$store_feed_status['completed_count'] / (int)$store_feed_status['products_count']) * 100, 2);
                    $status_for_client = $status_for_client . ' (completed '.$completed_percentage.'%)';
                }
                $this_store['feed_status'] = $status_for_client;
            } else {
                $storesSyncRequired = true;
            }

            $store_updates_status = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:updates_status", true);
            $remaining_updates = Mage::getModel('tagalys_core/queue')->getUpdatesCount($store_id);
            if ($this_store['setup_complete']) {
                if ($remaining_updates > 0) {
                    $storesSyncRequired = true;
                    $this_store['updates_status'] = $remaining_updates . ' remaining';
                } else {
                    if ($store_updates_status == null) {
                        $this_store['updates_status'] = 'Nothing to update';
                    } else {
                        switch($store_updates_status['status']) {
                            case 'generated_file':
                                $this_store['updates_status'] = 'Generated file. Sending to Tagalys.';
                                $storesSyncRequired = true;
                                break;
                            case 'sent_to_tagalys':
                                $this_store['updates_status'] = 'Waiting for Tagalys';
                                $waitingForTagalys = true;
                                break;
                            case 'finished':
                                $this_store['updates_status'] = 'Finished';
                                break;
                            case 'pending':
                                $this_store['updates_status'] = 'Nothing to update';
                                break;
                        }
                    }
                }
            } else {
                if ($remaining_updates > 0) {
                    $this_store['updates_status'] = 'Waiting for feed sync';
                } else {
                    $this_store['updates_status'] = 'Nothing to update';
                }
            }

            // categories
            $listing_pages_enabled = Mage::getModel('tagalys_core/config')->getTagalysConfig("module:listingpages:enabled");
            $totalEnabled = Mage::helper('tagalys_core/categories')->getEnabledCount($store_id);
            if ($listing_pages_enabled == '1' && $totalEnabled > 0) {
                $pendingSync = Mage::helper('tagalys_core/categories')->getPendingSyncCount($store_id);
                $requiringPositionsSync = Mage::helper('tagalys_core/categories')->getRequiringPositionsSyncCount($store_id);
                $listingPagesStatusMessages = array();
                if ($pendingSync > 0) {
                    array_push($listingPagesStatusMessages, 'Pending sync to Tagalys: '.$pendingSync);
                }
                if ($requiringPositionsSync > 0) {
                    array_push($listingPagesStatusMessages, 'Positions update required: ' . $requiringPositionsSync);
                    $indexerProcess = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
                    $indexerDone = ($indexerProcess->getStatus() == Mage_Index_Model_Process::STATUS_PENDING);
                    if (!$indexerDone) {
                        array_push($listingPagesStatusMessages, 'Category Products Index pending - reindex to update positions.');
                    }
                }
                if (empty($listingPagesStatusMessages)) {
                    array_push($listingPagesStatusMessages, 'Finished');
                }
                $this_store['listing_pages_status'] = implode(". ", $listingPagesStatusMessages);
            } else {
                $this_store['listing_pages_status'] = 'Not enabled';
            }

            $syncStatus['stores'][$store_id] = $this_store;
        }
        $syncStatus['client_side_work_completed'] = false;
        $config_sync_required = Mage::getModel('tagalys_core/config')->getTagalysConfig('config_sync_required');
        if ($storesSyncRequired == true || $config_sync_required == '1') {
            if ($storesSyncRequired == true) {
                $syncStatus['status'] = 'Stores Sync Pending';
            } else {
                if ($config_sync_required == '1') {
                    $syncStatus['status'] = 'Configuration Sync Pending';
                } else {
                    // should never come here
                    $syncStatus['status'] = 'Pending';
                }
            }
        } else {
            $syncStatus['client_side_work_completed'] = true;
            if ($waitingForTagalys) {
                $syncStatus['waiting_for_tagalys'] = true;
                $syncStatus['status'] = 'Waiting for Tagalys';
            } else {
                $syncStatus['status'] = 'Fully synced';
            }
        }

        if ($resyncScheduled) {
            $syncStatus['status'] = $syncStatus['status'] . '. Resync scheduled at 1 AM. You can resync manually by using the <strong>Trigger full products resync now</strong> option in the <strong>Support & Troubleshooting</strong> tab and then clicking on the <strong>Sync Manually</strong> button that will show below.';
        }

        return $syncStatus;
    }

    public function getOrderData($storeId, $from, $to=false){
        $from = date('Y-m-d H:i:s', $from);
        $orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('store_id',$storeId)->addAttributeToFilter('updated_at', ['from' => $from]);
        if($to){
            $to = date('Y-m-d H:i:s', $to);
            $orders->addAttributeToFilter('created_at', ['to' => $to]);
        }
        $data = [];
        $successStates = Mage::getModel('tagalys_core/config')->getTagalysConfig('success_order_states', true);
        foreach($orders as $order){
            if (in_array($order->getState(), $successStates)){
                $items = $order->getAllVisibleItems();
                foreach($items as $item){
                    $product = $this->getProductFromItem($item);
                    $data[] = [
                        'order_id' => $order->getId(),
                        'order_status' => $order->getStatus(),
                        'order_state' => $order->getState(),
                        'item_sku' => $item->getSku(),
                        'product_sku' => $product->getSku(),
                        'qty' => $item->getQtyOrdered(),
                        'user_id' => $order->getCustomerId(),
                        'timestamp' => $order->getCreatedAt(),
                    ];
                }
            }
        }
        return $data;
    }

    public function getProductFromItem($item) {
        $product = $item->getProduct();
        if($product->getVisibility() != '1') {
            return $product;
        }
        $configurableType = Mage::getModel('catalog/product_type_configurable');
        $parentIds = $configurableType->getParentIdsByChild($product->getId());
        if(count($parentIds) > 0) {
            $parent = Mage::getModel('catalog/product')->load($parentIds[0]);
            if($parent->getVisibility() != '1') {
                return $parent;
            }
            $childrenIds = $configurableType->getChildrenIds($product->getId());
            foreach($childrenIds as $childId) {
                $child = Mage::getModel('catalog/product')->load($parentIds[0]);
                if ($child->getVisibility() != '1') {
                    return $child;
                }
            }
        }
        return $product;
    }

    public static function forEachChunk($array, $chunkSize, $callback) {
        $thisBatch = array_splice($array, 0, $chunkSize);
        while(count($thisBatch) > 0) {
            $callback($thisBatch);
            $thisBatch = array_splice($array, 0, $chunkSize);
        }
    }
}