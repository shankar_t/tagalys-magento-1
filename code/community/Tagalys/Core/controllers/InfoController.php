<?php
class Tagalys_Core_InfoController extends Mage_Core_Controller_Front_Action {

    public function _checkPrivateIdentification($identification) {
        $api_credentials = Mage::getModel('tagalys_core/config')->getTagalysConfig('api_credentials', true);
        return ($identification['client_code'] == $api_credentials['client_code'] && $identification['api_key'] == $api_credentials['private_api_key']);
    }

    public function indexAction() {
        $params = $this->getRequest()->getParams();

        if ($this->_checkPrivateIdentification($params['identification'])) {
            switch($params['info_type']) {
                case 'status':
                    try {
                        $info = array('config' => array(), 'files_in_media_folder' => array(), 'sync_status' => Mage::helper('tagalys_core')->getSyncStatus());
                        $config_collection = Mage::getResourceModel('tagalys_core/config_collection');
                        foreach($config_collection as $i) {
                            $info['config'][$i->getData('path')] = $i->getData('value');
                        }
                        $media_directory = Mage::getBaseDir('media'). DS .'tagalys';
                        $files_in_media_directory = scandir($media_directory);
                        foreach ($files_in_media_directory as $key => $value) {
                            if (!is_dir($media_directory . DS . $value)) {
                                if (!preg_match("/^\./", $value)) {
                                    $info['files_in_media_folder'][] = $value;
                                }
                            }
                        }
                        $response = $info;
                    } catch (Exception $e) {
                        Mage::getSingleton('tagalys_core/client')->log('error', 'Error in indexAction: ' . $e->getMessage(), array('params' => $params));
                        $redirect_to_tab = 'api_credentials';
                    }
                    break;
                case 'mpages_cache':
                    $cache = array();
                    $cache_collection = Mage::getResourceModel('tagalys_mpages/cache_collection');
                    foreach($cache_collection as $i) {
                        array_push($cache, array('id' => $i->getData('id'), 'store_id' => $i->getData('store_id'), 'url' => $i->getData('url'), 'data' => $i->getData('data')));
                    }
                    $response = $cache;
                    break;
                case 'product_details':
                    $productDetails = array();
                    foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
                        $productDetails[$store_id] = array();
                        foreach($params['product_ids'] as $product_id) {
                            $productDetailsForStore = (array) Mage::helper("tagalys_core/service")->getProductPayload($product_id, $store_id, ($params['force_regenerate_thumbnails'] == 'true'));
                            array_push($productDetails[$store_id], $productDetailsForStore);
                        }
                    }
                    $response = $productDetails;
                    break;
                case 'reset_sync_statuses':
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
                        $sync_types = array('updates', 'feed');
                        foreach($sync_types as $sync_type) {
                          $sync_type_status = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:" . $sync_type . "_status", true);
                          $sync_type_status['status'] = 'finished';
                          $feed_status = Mage::getModel('tagalys_core/config')->setTagalysConfig("store:$store_id:" . $sync_type . "_status", json_encode($sync_type_status));
                        }
                    }
                    $response = array('reset' => true);
                    break;
                case 'trigger_full_product_sync':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Triggering full products resync via API', array('force_regenerate_thumbnails' => ($params['force_regenerate_thumbnails'] == 'true')));
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
                        if (isset($params['products_count'])) {
                            Mage::helper("tagalys_core/SyncFile")->triggerFeedForStore($store_id, (($params['force_regenerate_thumbnails'] == 'true')), $params['products_count']);
                        } else {
                            Mage::helper("tagalys_core/SyncFile")->triggerFeedForStore($store_id, (($params['force_regenerate_thumbnails'] == 'true')));
                        }
                    }
                    $response = array('triggered' => true);
                    break;
                case 'insert_into_sync_queue':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Inserting into sync queue via API', array('product_ids' => $params['product_ids']));
                    foreach($params['product_ids'] as $product_id) {
                        Mage::getModel('tagalys_core/queue')->blindlyAddProduct($product_id);
                    }
                    $response = array('inserted' => true);
                    break;
                case 'truncate_sync_queue':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Truncating sync queue via API');
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    $response = array('truncated' => true);
                    break;
                case 'update_mpages_cache':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Updating Merchandised Pages cache via API');
                    Mage::helper("tagalys_mpages")->updateMpagesCache();
                    $response = array('updated' => true);
                    break;
                case 'update_specific_mpage_cache':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Updating specific Merchandised Page cache via API', array('params' => $params));
                    $renderingMethod = Mage::getModel('tagalys_core/config')->getTagalysConfig("listing_pages:rendering_method");
                    if (($params['platform'] === true || $params['platform'] === 'true') && $renderingMethod == 'platform') {
                        Mage::helper('tagalys_core/categories')->markPositionsSyncRequired($params['identification']['store_id'], explode('-', $params['id'])[1]);
                        $response = array('markPositionsSyncRequired' => true);
                    } else {
                        $params_for_model = Mage::helper('tagalys_mpages')->saveCacheData($params);
                        $response = array('updated' => true, 'params_for_model' => $params_for_model);
                    }
                    break;
                case 'delete_specific_mpage_cache':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Deleting specific Merchandised Page cache via API', array('params' => $params));
                    $deleted = Mage::helper('tagalys_mpages')->deleteCacheData($params);
                    $response = array('deleted' => $deleted);
                    break;
                case 'mark_positions_sync_required_for_categories':
                    Mage::helper('tagalys_core/categories')->markPositionsSyncRequiredForCategories($params['identification']['store_id'], $params['category_ids']);
                    $response = array('updated' => true);
                    break;
                case 'update_category_pages_store_mapping':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('category_pages_store_mapping', $params['store_mapping'], true);
                    $response = array('updated' => true);
                    break;
                case 'get_categories_powered_by_tagalys':
                    $categories = array();
                    $categories_collection = Mage::getResourceModel('tagalys_core/categories_collection');
                    foreach($categories_collection as $i) {
                        $fields = array('id', 'category_id', 'store_id', 'positions_synced_at', 'positions_sync_required', 'marked_for_deletion', 'status');
                        $category_data = array();
                        foreach($fields as $field) {
                            $category_data[$field] = $i->getData($field);
                        }
                        array_push($categories, $category_data);

                    }
                    $response = array('categories' => $categories);
                    break;
                case 'update_tagalys_category_table':
                    $tagalysCategoryItem = Mage::getModel('tagalys_core/categories')->getCollection()
                        ->addFieldToFilter('store_id', $params['store_id'])
                        ->addFieldToFilter('category_id', $params['category_id'])
                        ->getFirstItem();
                    if ($tagalysCategoryItem->getId() == null) {
                        $tagalysCategoryItem = Mage::getModel('tagalys_core/categories');
                        $tagalysCategoryItem->addData([
                            'store_id' => $params['store_id'],
                            'category_id' => $params['category_id'],
                            'positions_sync_required' => 0,
                            'marked_for_deletion' => 0,
                            'status' => 'pending_sync',
                        ]);
                    }
                    $tagalysCategoryItem->addData($params['data'])->save();
                    $response = array('status' => 'OK', 'updated' => true, 'id' => $tagalysCategoryItem->getId());
                    break;
                case 'delete_tagalys_category_item':
                    $tagalysCategoryItem = Mage::getModel('tagalys_core/categories')->getCollection()
                        ->addFieldToFilter('store_id', $params['store_id'])
                        ->addFieldToFilter('category_id', $params['category_id'])
                        ->getFirstItem();
                    if ($tagalysCategoryItem->getId() != null) {
                        $tagalysCategoryItem->delete();
                        $response = array('status' => 'OK', 'deleted' => true, 'found' => true, 'id' => $tagalysCategoryItem->getId());
                    } else {
                        $response = array('status' => 'OK', 'deleted' => false, 'found' => false);
                    }
                    break;
                case 'update_tagalys_health_status':
                    if (isset($params['value']) && in_array($params['value'], array('1', '0'))) {
                        Mage::getModel('tagalys_core/config')->setTagalysConfig("tagalys:health", $params['value']);
                    } else {
                        Mage::helper("tagalys_core")->updateTagalysHealth();
                    }
                    $response = array('health_status' => Mage::getModel('tagalys_core/config')->getTagalysConfig('tagalys:health'));
                    break;
                case 'get_tagalys_health_status':
                    $response = array('health_status' => Mage::getModel('tagalys_core/config')->getTagalysConfig('tagalys:health'));
                    break;
                case 'update_tagalys_plan_features':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('tagalys_plan_features', $params['plan_features'], true);
                    break;
                case 'create_category':
                    try{
                        Mage::log("create_category: params: ".json_encode($params), null, 'tagalys_api.log', true);
                        $categoryId = Mage::helper('tagalys_core/categories')->createCategory($params['store_id'], $params['category_details']);
                        $response = ['status'=> 'OK', 'category_id'=> $categoryId];
                    } catch(\Exception $e){
                        $response = ['status'=> 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'update_category':
                    try{
                        Mage::log("update_category: params: ".json_encode($params), null, 'tagalys_api.log', true);
                        $res = Mage::helper('tagalys_core/categories')->updateCategoryDetails($params['category_id'], $params['category_details']);
                        if($res) {
                            $response = ['status'=>'OK', 'message'=>$res];
                        } else {
                            $response = ['status'=>'error', 'message'=>'Unknown error occurred'];
                        }
                    } catch(\Exception $e){
                        $response = ['status'=> 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'delete_tagalys_category':
                    try{
                        Mage::log("delete_tagalys_category: params: ".json_encode($params), null, 'tagalys_api.log', true);
                        $res = Mage::helper('tagalys_core/categories')->deleteTagalysCategory($params['category_id']);
                        if($res) {
                            $response = ['status'=>'OK', 'message'=>$res];
                        } else {
                            $response = ['status'=>'error', 'message'=>'Unknown error occurred'];
                        }
                    } catch(\Exception $e){
                        $response = ['status'=> 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'assign_products_to_category_and_remove':
                    try{
                        Mage::log("assign_products_to_category_and_remove: storeId: {$params['identification']['store_id']} categoryId: {$params['category_id']}", null, 'tagalys_api.log', true);
                        if($params['product_positions'] == -1){
                            $params['product_positions'] = [];
                        }
                        $res = Mage::helper('tagalys_core/categories')->bulkAssignProductsToCategoryAndRemove($params['identification']['store_id'], $params['category_id'], $params['product_positions']);
                        if($res) {
                            $response = ['status'=>'OK', 'message'=>$res];
                        } else {
                            $response = ['status'=>'error', 'message'=>'Unknown error occurred'];
                        }
                    } catch(\Exception $e){
                        $response = ['status'=> 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'update_product_positions':
                    try{
                        Mage::log("update_product_positions: storeId: {$params['identification']['store_id']} categoryId: {$params['category_id']}", null, 'tagalys_api.log', true);
                        if($params['product_positions'] == -1){
                            $params['product_positions'] = [];
                        }
                        $res = Mage::helper('tagalys_core/categories')->performCategoryPositionUpdate($params['identification']['store_id'], $params['category_id'], $params['product_positions']);
                        if($res) {
                            $response = ['status'=>'OK', 'message'=>$res];
                        } else {
                            $response = ['status'=>'error', 'message'=>'Unknown error occurred'];
                        }
                    } catch(\Exception $e){
                        $response = ['status'=> 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'get_plugin_version':
                    $response = ['status' => 'OK', 'plugin_version' => Mage::getStoreConfig('tagalys/package/version')];
                    break;
                case 'ping':
                    $response = ['status' => 'OK', 'message' => 'pong'];
                    break;
                case 'get_tagalys_logs':
                    try{
                        if(empty($params['lines'])){
                            $params['lines'] = 10;
                        }
                        ob_start();
                        passthru('tail -n'. escapeshellarg($params['lines']).' var/log/tagalys_'.escapeshellarg($params['file']).'.log');
                        $response = ['status' => 'OK', 'message' => explode("\n",trim(ob_get_clean()))];
                    } catch(\Exception $e) {
                        $response = ['status' => 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'get_order_data':
                    try {
                        Mage::log("get_order_data: storeId: {$params['store_id']} from: {$params['from']} to: {$params['to']}", null, 'tagalys_api.log', true);
                        $res = Mage::helper('tagalys_core/data')->getOrderData($params['store_id'], $params['from'], $params['to']);
                        $response = ['status' => 'OK', 'message' => $res];
                    } catch (\Exception $e) {
                        $response = ['status' => 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'set_sync_file_ext':
                    try {
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('sync_file_ext', $params['ext']);
                        $response = ['updated' => true, 'message' => $params['ext'] ];
                    } catch (\Exception $e) {
                        $response = ['status' => 'error', 'message' => $e->getMessage()];
                    }
                    break;
                case 'get_all_attribute_codes':
                    try {
                        $attributeCodes = [];
                        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
                        foreach ($attributes as $attribute) {
                            $attributeCodes[] = $attribute->getAttributeCode();
                        }
                        $response = ['status' => 'OK', 'attributes' => $attributeCodes ];
                    } catch (\Exception $e) {
                        $response = ['status' => 'error', 'message' => $e->getMessage()];
                    }
                    break;
            }
            // Make sure the content type for this response is JSON
            $this->getResponse()->clearHeaders()->setHeader(
                'Content-type',
                'application/json'
            );
            // Set the response body / contents to be the JSON data
            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($response)
            );
        } else {
            Mage::getSingleton('tagalys_core/client')->log('warn', 'Invalid identification in infoAction', array('params' => $params));
        }
    }

}