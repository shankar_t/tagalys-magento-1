<?php
class Tagalys_Core_SyncfilesController extends Mage_Core_Controller_Front_Action {

    public function _checkPrivateIdentification($identification) {
        $api_credentials = Mage::getModel('tagalys_core/config')->getTagalysConfig('api_credentials', true);
        return ($identification['client_code'] == $api_credentials['client_code'] && $identification['api_key'] == $api_credentials['private_api_key']);
    }

    public function callbackAction() {
        $params = $this->getRequest()->getParams();

        $response = array('result' => false);

        if (isset($params['identification']) && $this->_checkPrivateIdentification($params['identification'])) {
            $split = explode('media/tagalys/', $params['completed']);
            $filename = $split[1];
            if (is_null($filename)) {
                $split = explode('media\/tagalys\/', $params['completed']);
                $filename = $split[1];
            }
            if (is_null($filename)) {
                Mage::getSingleton('tagalys_core/client')->log('error', 'Error in callbackAction. Unable to read filename', array('params' => $params));
            } else {
                Mage::helper('tagalys_core/SyncFile')->tagalysCallback($params['identification']['store_id'], $filename);
                $response = array('result' => true);
            }
        } else {
            Mage::getSingleton('tagalys_core/client')->log('warn', 'Invalid identification in callbackAction', array('params' => $params));
        }

        // Make sure the content type for this response is JSON
        $this->getResponse()->clearHeaders()->setHeader(
            'Content-type',
            'application/json'
        );
        // Set the response body / contents to be the JSON data
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }

}