<?php

class Tagalys_Core_Adminhtml_TagalysController extends Mage_Adminhtml_Controller_Action {

    private $platformDetailsToSend = [];

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('Tagalys/tagalys_core');
    }

    public function indexAction() {
        $this->_title('Tagalys Configuration');
        // $this->_getSession()->addNotice($this->__('Need help? Visit <a href="http://support.tagalys.com" target="_blank">http://support.tagalys.com</a>'));
        $this->loadLayout();
        $this->_setActiveMenu('Tagalys/core');
        $this->renderLayout();
    }

    public function saveAction() {
        $params = $this->getRequest()->getParams();
        if (!empty($params['tagalys_submit_action'])) {
            $result = false;
            $this->_helper = Mage::helper('tagalys_core');
            $redirect_to_tab = null;
            switch ($params['tagalys_submit_action']) {
                case 'Save API Credentials':
                    try {
                        $result = $this->_saveApiCredentials($params);
                        if ($result !== false) {
                            Mage::getSingleton('tagalys_core/client')->log('info', 'Saved API credentials', array('api_credentials' => $params['api_credentials']));
                            $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
                            if ($setup_status == 'api_credentials') {
                                Mage::getModel('tagalys_core/config')->setTagalysConfig('setup_status', 'sync_settings');
                            }
                        }
                        $redirect_to_tab = 'api_credentials';
                    } catch (Exception $e) {
                        Mage::getSingleton('tagalys_core/client')->log('error', 'Error in _saveApiCredentials: ' . $e->getMessage(), array('api_credentials' => $params['api_credentials']));
                        Mage::getSingleton('core/session')->addError("Sorry, something went wrong while saving your API credentials. Please <a href=\"mailto:cs@tagalys.com\">email us</a> so we can resolve this issue.");
                        $redirect_to_tab = 'api_credentials';
                    }
                    break;
                case 'Save & Continue to Sync':
                    try {
                        if (array_key_exists('search_box_selector', $params)) {
                            Mage::getModel('tagalys_core/config')->setTagalysConfig('search_box_selector', $params['search_box_selector']);
                            Mage::getModel('tagalys_core/config')->setTagalysConfig('suggestions_align_to_parent_selector', $params['suggestions_align_to_parent_selector']);
                        }
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('periodic_full_sync', $params['periodic_full_sync']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('product_image_attribute', $params['product_image_attribute']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('product_image_hover_attribute', $params['product_image_hover_attribute']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('max_product_thumbnail_width', $params['max_product_thumbnail_width']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('max_product_thumbnail_height', $params['max_product_thumbnail_height']);
                        if (count($params['stores_for_tagalys']) > 0) {
                            Mage::getSingleton('tagalys_core/client')->log('info', 'Starting configuration sync', array('stores_for_tagalys' => $params['stores_for_tagalys']));
                            $response = Mage::helper("tagalys_core/service")->syncClientConfiguration($params['stores_for_tagalys']);
                            if ($response === false || $response['result'] === false) {
                                Mage::getSingleton('tagalys_core/client')->log('error', 'syncClientConfiguration returned false', array('stores_for_tagalys' => $params['stores_for_tagalys']));
                                Mage::getSingleton('core/session')->addError("Sorry, something went wrong while saving your store's configuration. We've logged the issue and we'll get back once we know more. You can contact us here: <a href=\"mailto:cs@tagalys.com\">cs@tagalys.com</a>");
                                $redirect_to_tab = 'sync_settings';
                            } else {
                                Mage::getSingleton('tagalys_core/client')->log('info', 'Completed configuration sync', array('stores_for_tagalys' => $params['stores_for_tagalys']));
                                Mage::getModel('tagalys_core/config')->setTagalysConfig('stores', json_encode($params['stores_for_tagalys']));
                                foreach($params['stores_for_tagalys'] as $i => $store_id) {
                                    Mage::helper("tagalys_core/SyncFile")->triggerFeedForStore($store_id);
                                }
                                $setup_status = Mage::getModel('tagalys_core/config')->getTagalysConfig('setup_status');
                                if ($setup_status == 'sync_settings') {
                                    Mage::getModel('tagalys_core/config')->setTagalysConfig('setup_status', 'sync');
                                }
                                $redirect_to_tab = 'sync';
                            }
                        } else {
                            Mage::getSingleton('core/session')->addError("Please choose at least one store to continue.");
                            $redirect_to_tab = 'sync_settings';
                        }
                    } catch (Exception $e) {
                        Mage::getSingleton('tagalys_core/client')->log('error', 'Error in syncClientConfiguration: ' . $e->getMessage(), array('stores_for_tagalys' => $params['stores_for_tagalys']));
                        Mage::getSingleton('core/session')->addError("Sorry, something went wrong while saving your configuration. Please <a href=\"mailto:cs@tagalys.com\">email us</a> so we can resolve this issue.");
                        $redirect_to_tab = 'sync_settings';
                    }
                    break;
                case 'Save Search Settings':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('module:search:enabled', $params['enable_search']);
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('search_box_selector', $params['search_box_selector']);
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('suggestions_align_to_parent_selector', $params['suggestions_align_to_parent_selector']);
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'search:enabled:'.$params['enable_search']);
                    $redirect_to_tab = 'search';
                    break;
                case 'Save Merchandising Pages Settings':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('module:mpages:enabled', $params['enable_mpages']);
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'mpages:enabled:'.$params['enable_mpages']);
                    $redirect_to_tab = 'mpages';
                    break;
                case 'Save Listing Pages Settings':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('module:listingpages:enabled', $params['enable_listingpages']);
                    if ($params['enable_listingpages'] == '1' && $params['understand_and_agree'] == 'I agree') {
                        Mage::getSingleton('core/session')->addNotice("Settings have been saved. Selected categories will be visible in your Tagalys Dashboard within 10 minutes and product positions on these categories will be updated within 15 minutes unless specificed below.");
                        if (!array_key_exists('category_pages_rendering_method', $params)){
                            $params['category_pages_rendering_method'] = 'platform';
                        }
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:rendering_method', $params['category_pages_rendering_method']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:position_sort_direction', $params['position_sort_direction']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:understand_and_agree', $params['understand_and_agree']);
                        if ($params['category_pages_rendering_method'] == 'platform') {
                            $hasMultiStoreWarning = Mage::helper('tagalys_core/categories')->isMultiStoreWarningRequired();
                            Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:has_multi_store_warning', $hasMultiStoreWarning);
                            if (array_key_exists('same_or_similar_products_across_all_stores', $params)) {
                                Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:same_or_similar_products_across_all_stores', $params['same_or_similar_products_across_all_stores']);
                                Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:store_id_for_category_pages', $params['store_id_for_category_pages']);
                            }
                            Mage::getModel('tagalys_core/config')->setTagalysConfig('js_rendered_stores', json_encode($params['js_rendered_stores']));
                        }
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:override_layout', $params['override_layout_for_listing_pages']);
                        Mage::getModel('tagalys_core/config')->setTagalysConfig('listing_pages:override_layout_name', $params['override_layout_name_for_listing_pages']);
                        foreach($params['stores_for_tagalys'] as $storeId) {
                            if (
                                $params['category_pages_rendering_method'] == 'platform' &&
                                array_key_exists('same_or_similar_products_across_all_stores', $params) && $params['same_or_similar_products_across_all_stores'] == '1' &&
                                $storeId.'' != $params['store_id_for_category_pages'].''
                            ) {
                                Mage::getModel("tagalys_core/categories")->markStoreCategoryIdsForDeletionExcept($storeId, array());
                                continue;
                            }
                            if($params["enable_smart_page_store_$storeId"] == 1){
                                Mage::getModel('tagalys_core/config')->setTagalysConfig("enable_smart_page_store_$storeId", 1);
                                if($params["smart_page_parent_category_name_store_$storeId"] == ""){
                                    $params["smart_page_parent_category_name_store_$storeId"] = 'Tagalys';
                                }
                                if($params["smart_page_parent_category_url_key_store_$storeId"] == ""){
                                    $params["smart_page_parent_category_url_key_store_$storeId"] = 'buy';
                                }
                                try{
                                    $this->saveSmartPageParentCategory($storeId, $params);
                                } catch(\Exception $e) {
                                    Mage::getSingleton('core/session')->addError("An error occurred while saving your Smart Category setting");
                                    Mage::log("saveSmartPageParentCategory: error: {$e->getMessage()} backtrace:".json_encode($e->getTrace()), null, 'tagalys_categories.log', true);
                                }
                            } else {
                                Mage::getModel('tagalys_core/config')->setTagalysConfig("enable_smart_page_store_$storeId", 0);
                                // this is to disable new page creation in Tagalys dashboard
                                $this->platformDetailsToSend['parent_category_id'] = null;
                                $this->platformDetailsToSend['parent_category_name'] = null;
                            }
                            $currentStore = Mage::app()->getStore();
                            Mage::app()->setCurrentStore(Mage::app()->getStore($storeId));
                            $this->platformDetailsToSend['js_rendered_category_pages'] = in_array($storeId, $params['js_rendered_stores']);
                            $this->platformDetailsToSend['platform_pages_rendering_method'] = $params['category_pages_rendering_method'];
                            Mage::getSingleton('tagalys_core/client')->storeApiCall($storeId . '', '/v1/stores/update_platform_details', ['platform_details' => $this->platformDetailsToSend]);
                            if (!array_key_exists('categories_for_tagalys_store_' . $storeId, $params)) {
                                $params[ 'categories_for_tagalys_store_' . $storeId] = array();
                            }
                            $categoryIds = array();
                            if (count($params['categories_for_tagalys_store_'. $storeId]) > 0) {
                                foreach($params['categories_for_tagalys_store_' . $storeId] as $categoryPath) {
                                    $path = explode('/', $categoryPath);
                                    $categoryIds[] = intval(end($path));
                                }
                                foreach ($categoryIds as $categoryId) {
                                    $category = Mage::getModel('catalog/category')->load($categoryId);
                                    if ($category->getDisplayMode() == Mage_Catalog_Model_Category::DM_PAGE) {
                                        // skip
                                        $firstItem = Mage::getModel('tagalys_core/categories')->getCollection()
                                            ->addFieldToFilter('store_id', $storeId)
                                            ->addFieldToFilter('category_id', $categoryId)
                                            ->getFirstItem();
                                        if ($id = $firstItem->getId()) {
                                            $firstItem->addData(array('marked_for_deletion' => 1))->save();
                                        }
                                    } else {
                                        Mage::getModel("tagalys_core/categories")->createOrUpdateWithData($storeId, $categoryId, array('positions_sync_required' => 0, 'marked_for_deletion' => 0, 'status' => 'pending_sync'), array('marked_for_deletion' => 0));
                                    }
                                }
                            }
                            Mage::getModel("tagalys_core/categories")->markStoreCategoryIdsForDeletionExcept($storeId, $categoryIds);
                            Mage::app()->setCurrentStore($currentStore);
                        }
                    } else {
                        if ($params['enable_listingpages'] == '1') {
                            Mage::getSingleton('core/session')->addError("Settings have not been updated because you did not type 'I agree'.");
                        }
                    }
                    $redirect_to_tab = 'listingpages';
                    break;
                case 'Save Recommendations Settings':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('module:recommendations:enabled', $params['enable_recommendations']);
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'recommendations:enabled:'.$params['enable_recommendations']);
                    $redirect_to_tab = 'recommendations';
                    break;
                case 'Save My Store Settings':
                    Mage::getModel('tagalys_core/config')->setTagalysConfig('module:mystore:enabled', $params['enable_mystore']);
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'mystore:enabled:'.$params['enable_mystore']);
                    $redirect_to_tab = 'mystore';
                    break;
                case 'Update positions for all categories':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Updating positions for all categories');
                    Mage::helper('tagalys_core/categories')->markPositionsSyncRequiredForCategories('all', 'all');
                    $redirect_to_tab = 'support';
                    break;
                case 'Retry syncing failed categories':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Retrying failed categories sync');
                    Mage::helper('tagalys_core/categories')->markFailedCategoriesForRetrying();
                    $redirect_to_tab = 'support';
                    break;
                case 'Trigger full products resync now':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Triggering full products resync');
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    $triggered = true;
                    $force_trigger = true;
                    foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
                        $store_triggered = Mage::helper("tagalys_core/SyncFile")->triggerFeedForStore($store_id, true, false, true, $force_trigger);
                        if (!$store_triggered) {
                            $triggered = false;
                        }
                    }
                    if (!$triggered) {
                        Mage::getSingleton('core/session')->addError("Unable to trigger a full resync. There is already a sync in progress.");
                    }
                    $redirect_to_tab = 'support';
                    break;
                case 'Trigger configuration resync now':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Triggering configuration resync');
                    Mage::getModel('tagalys_core/config')->setTagalysConfig("config_sync_required", '1');
                    $redirect_to_tab = 'support';
                    break;
                case 'Truncate Tagalys sync queue now':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Truncating Tagalys sync queue');
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    $redirect_to_tab = 'support';
                    break;
                case 'Update Popular Searches now':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Triggering update popular searches');
                    Mage::helper("search_suggestions")->cachePopularSearches();
                    $redirect_to_tab = 'support';
                    break;
                case 'Update Merchandised Pages cache now':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Triggering update mpages cache');
                    Mage::helper("tagalys_mpages")->updateMpagesCache();
                    $redirect_to_tab = 'support';
                    break;
                case 'Restart Tagalys Setup':
                    Mage::getSingleton('tagalys_core/client')->log('warn', 'Restarting Tagalys Setup');
                    Mage::getResourceModel('tagalys_core/queue')->truncate();
                    Mage::getResourceModel('tagalys_core/config')->truncate();
                    Mage::getResourceModel('tagalys_core/categories')->truncate();
                    $redirect_to_tab = 'api_credentials';
                    break;
                case 'Update Tagalys API status now':
                    Mage::helper("tagalys_core")->updateTagalysHealth();
                    $redirect_to_tab = 'support';
                    break;
            }
            return $this->_redirect('*/tagalys', array('_query' => 'tab='.$redirect_to_tab));
        }
    }

    public function syncmanuallyAction() {
        Mage::helper("tagalys_core/SyncFile")->sync(25, 10);
        $syncStatus = Mage::helper('tagalys_core')->getSyncStatus();
        $this->getResponse()->setBody(json_encode($syncStatus));
    }
    public function syncstatusAction() {
        $syncStatus = Mage::helper('tagalys_core')->getSyncStatus();
        $this->getResponse()->setBody(json_encode($syncStatus));
    }

    protected function _saveApiCredentials($params) {
        $tagalys_api_client = Mage::getSingleton("tagalys_core/client");
        $result = $tagalys_api_client->identificationCheck(json_decode($params['api_credentials'], true));
        if ($result['result'] != 1) {
            Mage::getSingleton('core/session')->addError("Invalid API Credentials. Please try again. If you continue having issues, please email us <a href=\"mailto:cs@tagalys.com\">email us</a>.");
            return false;
        }
        // save credentials
        Mage::getModel('tagalys_core/config')->setTagalysConfig('api_credentials', $params['api_credentials']);
        Mage::getSingleton('tagalys_core/client')->cacheApiCredentials();
        return true;
    }

    private function saveSmartPageParentCategory($storeId, $params) {
        $categoryId = Mage::getModel('tagalys_core/config')->getTagalysConfig("tagalys_parent_category_store_$storeId");
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $categoryId = $category->getId();
        $categoryDetails = [];
        $categoryDetails['name'] = $params["smart_page_parent_category_name_store_$storeId"];
        if($categoryId) {
            Mage::helper('tagalys_core/categories')->updateCategoryDetails($categoryId, $categoryDetails);
            $urlKey = $category->getUrlKey();
        } else {
            $categoryDetails['url_key'] = strtolower($params["smart_page_parent_category_url_key_store_$storeId"]);
            $categoryId = Mage::helper('tagalys_core/categories')->createTagalysParentCategory($storeId, $categoryDetails);
        }
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $this->platformDetailsToSend['parent_category_id'] = $category->getId();
        $this->platformDetailsToSend['parent_category_name'] = $category->getName();
        $urlKey = $category->getUrlKey();
        $urlSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix', $storeId);
        Mage::getSingleton('tagalys_core/client')->clientApiCall('/v1/mpages/update_base_url', ['url_key' => $urlKey, 'store_id' => $storeId, 'url_suffix' => $urlSuffix]);
    }
}