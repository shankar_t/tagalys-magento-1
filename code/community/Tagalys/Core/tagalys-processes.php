<?php

if (isset($argv)) {
  // called from command line.

  // initialize Magento
  chdir(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
  require 'app/Mage.php';
  $_SERVER['SCRIPT_NAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_NAME']);
  $_SERVER['SCRIPT_FILENAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_FILENAME']);
  Mage::app();

  Mage::log("starting {$argv[1]};", null, 'tagalys_processes.log', true);

  $utc_now = new DateTime("now", new DateTimeZone('UTC'));
  $time_now = $utc_now->format(DateTime::ATOM);

  switch ($argv[1]) {
    case 'sync':
      // suggested cron: every five minutes
      // example cron: */5 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php sync 200 50
      // second parameter is number of products to write to file in a single cron execution
      // third parameter is number of category pages sync in a single cron execution
      // for maximum speed, set the numbers to the maximum that both processes will finish within cron frequency
      ini_set("memory_limit", "512M");
      Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:process:sync", $time_now);
      Mage::helper("tagalys_core/SyncFile")->sync(intval($argv[2]), intval($argv[3]));
      break;
    case 'update-product-positions':
      // suggested cron: every five minutes
      // example cron: */5 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php update-product-positions
      Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:process:update-product-positions", $time_now);
      $maxCategories = (isset($argv[2]) ? intval($argv[2]) : 50);
      Mage::helper('tagalys_core/categories')->updatePositionsIfRequired($maxCategories);
      break;
    case 'run-maintenance':
      // suggested cron: once a day at a low traffic time
      // example cron: 0 2 * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php run-maintenance
      Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:process:run-maintenance", $time_now);
      Mage::helper('tagalys_core/SyncFile')->runMaintenance();
      break;
    case 'update-caches':
      // suggested cron: every hour
      // example cron: 0 * * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php update-caches
      Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:process:update-caches", $time_now);
      Mage::helper("search_suggestions")->cachePopularSearches();
      break;
    case 'trigger-full-sync':
      // suggested cron: once a day at a low traffic time
      // example cron: 0 2 * * * php -f /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php trigger-full-sync
      Mage::getModel('tagalys_core/config')->setTagalysConfig("heartbeat:process:trigger-full-sync", $time_now);
      Mage::getResourceModel('tagalys_core/queue')->truncate();
      foreach (Mage::helper('tagalys_core')->getStoresForTagalys() as $store_id) {
        $sync_types = array('updates', 'feed');
        foreach ($sync_types as $sync_type) {
          $sync_type_status = Mage::getModel('tagalys_core/config')->getTagalysConfig("store:$store_id:" . $sync_type . "_status", true);
          $sync_type_status['status'] = 'finished';
          $feed_status = Mage::getModel('tagalys_core/config')->setTagalysConfig("store:$store_id:" . $sync_type . "_status", json_encode($sync_type_status));
        }
        Mage::helper("tagalys_core/SyncFile")->triggerFeedForStore($store_id);
      }
      break;
    case 'assign-parent-categories-to-all-products':
      // not to be called periodically. call only once when setting up and when required if parent category assignment has been missed out.
      // /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php assign-parent-categories-to-all-products
      Mage::helper("tagalys_core")->assignParentCategoriesToAllProducts();
      $indexerProcess = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
      $indexerProcess->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
      break;
    case 'onetime-transition-from-categories-config':
      // not to be called periodically. call only once when setting up
      // /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php onetime-transition-from-categories-config
      Mage::helper('tagalys_core/categories')->transitionFromCategoriesConfig();
      break;
    case 'onetime-transition-sync-all-categories':
      // not to be called periodically. call only once when setting up
      // /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php onetime-transition-sync-all-categories
      Mage::helper('tagalys_core/categories')->syncAll(true);
      break;
    case 'onetime-transition-force-update-all-product-positions':
      // not to be called periodically. call only once when setting up to force position updates when listing pages by tagalys is disabled
      // /path_to_magento_root/app/code/community/Tagalys/Core/tagalys-processes.php onetime-transition-force-update-product-positions 1000
      // second parameter is number of categories to update positions
      Mage::helper('tagalys_core/categories')->updatePositionsIfRequired(intval($argv[2]), 5, true);
      break;
    case 'onetime-transition-mpages':
      $keepOldUrl = intval($argv[2]);
      $storeIds = json_decode($argv[3]);
      Mage::helper('tagalys_core/categories')->transitionMpages($keepOldUrl, $storeIds);
      break;
  }
  Mage::log("ending {$argv[1]};", null, 'tagalys_processes.log', true);
} else {
  // web context. don't run.
  Mage::log("failed {$argv[1]};", null, 'tagalys_processes.log', true);
}
